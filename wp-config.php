<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'liyana_courses');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X{<JmU6$eX.%5!{.P4oPhVLbrKk:u_g4H2dxR#P(YIUt=M~$0[J0fZ]3wp8P&w@J');
define('SECURE_AUTH_KEY',  '[aWd&0th(NG^X}0I:e}W8KdAs/^gVfuq}6z<ub$usAwWW;*xI HN=9D#|W8{C)pZ');
define('LOGGED_IN_KEY',    'b~ev8IO=A;F/]0t:xts`=wsEd7EYxUy$.bPs08Wru,j9pR(Xm9SI%(x @Y-HYuEY');
define('NONCE_KEY',        '6c9UHY8nI-&+C{`!h#*Fkh{N.{^wDb~%5he58(02-V=%b(+z-Oq!}0h]Dod-o)m1');
define('AUTH_SALT',        ' I+a.K4QHLN+]nu$mgCmv]%E^xD:`;)DQEHvtN5^gA#pPpeK{s /-Wcvvx}l^W]F');
define('SECURE_AUTH_SALT', 'w?F`efT;TN=D@ss}tQ+,W`-9WBM1V#U}96).u#$o@vuf-x{6lu:2.smu%azE&Kws');
define('LOGGED_IN_SALT',   ';-6l>ToHNFq.Sk(HOON#T^bsR!7iFcf*(HmXiH]^Vy>I/0,cnjk#dcAZO@k;1.l;');
define('NONCE_SALT',       '(B;<n25KMP0L$L^;<Sw5(Te/wjRg[H_{A )`YFBAnHaU=?-Z!4etLYeMM)I0L;n3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define('WP_DEBUG_LOG', true);
define( 'WP_DEBUG_DISPLAY', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
