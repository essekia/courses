(function($) {
    'use strict';

    /**
     * All of the code for your public-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */



    $(document).ready(function() {
        console.log("jQuery loaded");

        helpieSyntaxHighlight();

    //     $(".pauple-helpie-single-sidebar ol.level3 ").mouseover(function() {
    //
    //         $(this).addClass('animate pulse');
    //
    //     }).mouseout(function() {
    //         $(this).addClass('animate');
    //
    // });

    $('#autocomplete').autocomplete({
        lookup: function(query, done) {
            // Do Ajax call or lookup locally, when done,
            // call the callback and pass your results:

            var nonce = HelpieSearchNonce;

            var data = {
                'action': 'helpie_search_autosuggest',
                'nonce': nonce,
                'query_value': query // We pass php values differently!
            };
            // console.log('my_ajax_object.ajax_url: ' + my_ajax_object.ajax_url);
            // We can also pass the url value separately from ajaxurl for front end AJAX implementations
            jQuery.post(my_ajax_object.ajax_url, data, function(response) {
                // console.log('Got this from the server: ' + response[0]);

                var ajaxResponse = JSON.parse(response);
                console.log("from server: : " + JSON.stringify(ajaxResponse, null, 5));

                var result = {
                    suggestions: ajaxResponse
                };

                done(result);
            });



        },
        onSelect: function(suggestion) {
            console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
        }
    });

    $('.helpie-main-content-area ul.main-nav li a').click(function() {
        $('.helpie-main-content-area ul.main-nav li').removeClass('active');
        $(this).closest('li').addClass('active');
        $('.helpie-main-content-area .content-section').hide();
        $($(this).data("target")).show();
    });

    $('.category-sidebar ul li a').click(function() {
        console.log('category-sidebar link clicked: ' + $('.category-main-content .content-section').size());

        $('.category-sidebar ul li').removeClass('active');
        $(this).closest('li').addClass('active');
        $('.category-main-content .category-section').hide();
        $($(this).data("target")).show();
    });



    $('.pauple-helpie-single-sidebar .cat-toggle ol.level1 > li').click(function() {
      // console.log('sidebar category toggle');

      $(this).addClass('active');
      var visibility = 'hidden';
      if($(this).next('.primary-term-children').is(":visible")){
            visibility = 'visible';
        } else{
        }

      $('.pauple-helpie-single-sidebar .primary-term-children').hide();
      $('.pauple-helpie-single-sidebar ol.level1 > li').removeClass('active');

      if(visibility == 'visible'){

        $(this).next('.primary-term-children').show();
      }else{
        $(this).addClass('active');
      }
      $(this).next('.primary-term-children').toggle();

    });

    jQuery('.helpie-single-page-module.page-scroll-module').css({
        'height': $(window).height(),
    });




});

// jQuery('.helpie-copy-to-clipboard').click(function() {
//     var ID = $(this).attr('id');
//     helpieCopyfieldvalue(event, ID);
// });

function scrollToElementOnClick() {
    $('.page-scroll-nav ol.current-ol li:first-child').addClass('active');


    $('.page-scroll-nav ol.level3 > li a').click(function() {
        $('.page-scroll-nav ol li').removeClass('active');
        $(this).closest('li').addClass('active');

        var scrollValue = parseInt($($(this).attr('href')).position().top);
        console.log('scrollValue: ' + scrollValue);
        $('#primary').animate({
            scrollTop: scrollValue
        }, 500);
        return false;
    });
}



function hidePageScrollNav() {
    var scrollLabel = '.pauple-helpie-single-sidebar .page-scroll-nav';
    if ($(scrollLabel + ' ol li').length == 0) {
        $(scrollLabel).hide();
    }
}

hidePageScrollNav();

scrollToElementOnClick();

$('.pauple-helpie-module.article-voting .voting-icon').click(function() {
    var previous_value = '';
    var previous_element = '';
    if ($('.pauple-helpie-module.article-voting .voting-icon.selected').length) {
        previous_value = $('.pauple-helpie-module.article-voting .voting-icon.selected').attr('data-vote');
        previous_element = $('.pauple-helpie-module.article-voting .voting-icon.selected');
    }

    var value = $(this).attr('data-vote');
    var postID = $(this).closest('.icon-tray').attr('data-post-id');
    var userID = $(this).closest('.icon-tray').attr('data-user-id');
    console.log(postID);

    $('.pauple-helpie-module.article-voting .voting-icon').removeClass('selected');
    $(this).addClass('selected');
    articleVote(value, postID, userID);
    if (value != previous_value) {
        var previous_count = parseInt(previous_element.find('count').text());
        var new_count = previous_count - 1;
        previous_element.find('count').text(new_count);

        var new_pc = parseInt($(this).find('count').text());
        var new_nc = new_pc + 1;
        $(this).find('count').text(new_nc);
    }
});

function articleVote(value, postID, userID) {
    var nonce = ArticleVoteNonce;

    var data = {
        'action': 'article_voting_callback',
        'nonce': nonce,
        'voteValue': value, // We pass php values differently!
        'postID': postID,
        'userID': userID,
        // 'postID': get_the_ID(),
    };


    jQuery.post(my_ajax_object.ajax_url, data, function(response) {
        var ajaxResponse = JSON.parse(response);
        console.log("from server: : " + JSON.stringify(ajaxResponse, null, 5));

        var result = {
            suggestions: ajaxResponse
        };

    });


}

/* Helpie Syntax Highlighter */
function helpieSyntaxHighlight() {
    // hljs.initHighlightingOnLoad();
    hljs.configure({
        tabReplace: '    ', // 4 spaces
        classPrefix: '' // don't append class prefix
            // … other options aren't changed
    })
    hljs.initHighlighting();
    var clipboard = new Clipboard('.helpie-copy-icon');
    // clipboard.on('success', function(e) {
    //     console.info('Action:', e.action);
    //     console.info('Text:', e.text);
    //     console.info('Trigger:', e.trigger);
    //     // $(e.trigger).children('.label').html('copied!');
    //     e.clearSelection();
    // });
    //
    // clipboard.on('error', function(e) {
    //     console.error('Action:', e.action);
    //     console.error('Trigger:', e.trigger);
    // });

    $('.helpie-syntax-highlight code').each(function(index, element) {
        var codeID = $(this).attr('id');
        $(this).after("<a class='helpie-copy-icon' data-clipboard-target='#" + codeID + "'><span class='label'>copy to clipboard</span><i class='fa fa-clipboard' aria-hidden='true'></i></a>")
        var code_content = $(this).html();
        // code_content = helpie_htmlDecode(code_content);
        $(this).html(code_content);
    });



}

function helpie_htmlDecode(input) {
    var output = $('<div/>').text(input).html();
    return output;
}


function helpieCopyfieldvalue(e, id) {
    var field = document.getElementById(id);
    console.log('ID: ' + id);
    var str = JSON.stringify(field);
    var str = JSON.stringify(field, null, 4); // (Optional) beautiful indented output.
    field.focus();
    console.log('field: ' + str);
    field.setSelectionRange(0, field.value.length);
    var copysuccess = copySelectionText();
    if (copysuccess) {
        showtooltip(e);
    }
}




})(jQuery);
