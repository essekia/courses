<?php

/**
 * The plugin bootstrap file.
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://pauple.com
 * @since             1.0.0
 *
 * @wordpress-plugin
 * Plugin Name:       VideoPress - WordPress PilotPress supplement for videos
 * Plugin URI:        http://pauple.com/videopress/
 * Description:       VideoPress is a WordPress helpdesk plugin.
 * Version:           0.1
 * Author:            Pauple Studios
 * Author URI:        http://pauple.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pauple-videopress
 * Domain Path:       /languages
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    global $pauple_helpie_plugin_version;
    $pauple_helpie_plugin_version = '0.1';




 /**
  * The core plugin class that is used to define internationalization,
  * admin-specific hooks, and public-facing site hooks.
  */


 /**
  * Begins execution of the plugin.
  *
  * Since everything within the plugin is registered via hooks,
  * then kicking off the plugin from this point in the file does
  * not affect the page life cycle.
  *
  * @since    1.0.0
  */

include_once 'video-nav.php';

 class PAUPLE_VIDEOPRESS_MAIN_CLS{

     private $video_nav;

     public function __construct(){
          $this->video_nav = new PAUPLE_VIDEOPRESS_VIDEO_NAV();
     }

     public static function activate_plugin_name()
     {
         require_once plugin_dir_path(__FILE__).'includes/core/class-helpie-activator.php';
         PAUPLE_HELPIE_ACTIVATOR::activate();
     }

     public static function deactivate_plugin_name()
     {
         require_once plugin_dir_path(__FILE__).'includes/core/class-helpie-deactivator.php';
         PAUPLE_HELPIE_DEACTIVATOR_CLS::deactivate();
     }

     public function run_plugin_name()
     {
       require plugin_dir_path(__FILE__).'includes/core/class-cpt.php';
       add_action( 'add_meta_boxes_page', array($this->video_nav, 'add_pages_metaboxes') );
       add_action( 'save_post', array($this->video_nav, 'save_pages_metaboxes1'), 10, 2 );

       wp_enqueue_style('pauple_videopress', plugin_dir_url(__FILE__).'public/css/videopress.css', array(), '1.0.1', null);


       add_shortcode('pauple_videopress_video_nav', array($this->video_nav, 'pauple_video_nav'));
     }

 } // End of class

$pauple_videopress_main = new PAUPLE_VIDEOPRESS_MAIN_CLS();
$pauple_videopress_main->run_plugin_name();
