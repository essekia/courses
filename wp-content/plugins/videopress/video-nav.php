<?php

include_once 'form-builder.php';
class PAUPLE_VIDEOPRESS_VIDEO_NAV{

    private $form_builder;
    public function __construct(){
        $this->form_builder = new PAUPLE_VIDEOPRESS_FORM_BUILDER();
    }
    public function save_pages_metaboxes1($post_id, $post){

      $this->save_pages_metaboxes($post_id, $post, 'pauple_videopress_nav1', 'videopress_video1');
      $this->save_pages_metaboxes($post_id, $post, 'pauple_videopress_nav1', 'videopress_video2');
      $this->save_pages_metaboxes($post_id, $post, 'pauple_videopress_nav1', 'videopress_video3');

      $this->save_pages_metaboxes($post_id, $post, 'pauple_videopress_nav1', 'videopress_video1_tag');
      $this->save_pages_metaboxes($post_id, $post, 'pauple_videopress_nav1', 'videopress_video2_tag');
      $this->save_pages_metaboxes($post_id, $post, 'pauple_videopress_nav1', 'videopress_video3_tag');

    }


     public function save_pages_metaboxes($post_id, $post, $nonce, $form_post_name){
            error_log("save_pages_metaboxes: ", 0);
            /* Verify the nonce before proceeding. */
            $nonce = 'pauple_'.$form_post_name;
            if ( !isset( $_POST[$nonce] ) || !wp_verify_nonce( $_POST[$nonce], basename( __FILE__ ) ) )
            return $post_id;

            /* Get the post type object. */
            $post_type = get_post_type_object( $post->post_type );

            /* Check if the current user has permission to edit the post. */
            if ( !current_user_can( $post_type->cap->edit_post, $post_id ) )
            return $post_id;

            /* Get the posted data and sanitize it for use as an HTML class. */
            $new_meta_value = ( isset( $_POST[$form_post_name] ) ? sanitize_html_class( $_POST[$form_post_name] ) : '' );
            error_log("new_meta_value: ".$new_meta_value, 0);
            /* Get the meta key. */
            $meta_key = $form_post_name;

            /* Get the meta value of the custom field key. */
            $meta_value = get_post_meta( $post_id, $meta_key, true );

            /* If a new meta value was added and there was no previous value, add it. */
            if ( $new_meta_value && '' == $meta_value )
            add_post_meta( $post_id, $meta_key, $new_meta_value, true );

            /* If the new meta value does not match the old value, update it. */
            elseif ( $new_meta_value && $new_meta_value != $meta_value )
            update_post_meta( $post_id, $meta_key, $new_meta_value );

            /* If there is no new meta value but an old value exists, delete it. */
            elseif ( '' == $new_meta_value && $meta_value )
            delete_post_meta( $post_id, $meta_key, $meta_value );

        }

        public function add_pages_metaboxes() {
            add_meta_box('pauple_video_nav1', 'Videos Navigation', array($this,'pauple_video_nav1'), 'page', 'normal', 'default');
        }

        public function pauple_video_nav1($post){

            $videos = array(
            0 => 'videopress_video1',
            1 => 'videopress_video2',
            2 => 'videopress_video3'
            );

            $count = 1;
            foreach($videos as $video){
            $this->pauple_video_nav_builder($post, $video, $count);
            $count++;
            }

        }

        public function single_video_page_dd($post, $id){
            $saved_value = get_post_meta(get_the_ID(), $id, true);

            $args = array(
            'post_type' =>'pauple_videopress',
            'post_status' => 'publish'
            );

            $options = array();
            $posts_array = get_posts($args);
            foreach ( $posts_array as $post1 ) : setup_postdata( $post1 );
                $options[$post1->ID] = $post1->post_title;
            endforeach;
            wp_reset_query();

            wp_nonce_field( basename( __FILE__ ), 'pauple_'.$id );
            $metabox_html = $this->form_builder->get_dropdown($id, $saved_value, $options);
            return $metabox_html;
        }


    public function single_video_tagname_dd($post, $id){
        $saved_value = get_post_meta(get_the_ID(), $id, true);
        $options = array();
        $args = array(
        'post_type' =>'pauple_videopress',
        'post_status' => 'publish'
        );
        $posts_array = get_posts($args);

        foreach ( $posts_array as $post1 ) : setup_postdata( $post1 );
        $tag = $post1->post_title."_tag";
        $options[$tag] = $tag;
        endforeach;
        wp_reset_query();

        wp_nonce_field( basename( __FILE__ ), 'pauple_'.$id );

        $metabox_html = $metabox_html = $this->form_builder->get_dropdown($id, $saved_value, $options);
        return $metabox_html;
    }

    public function pauple_video_nav_builder($post, $id, $count){

        $metabox_html = "<h3>Video".$count."</h3>";
        $metabox_html .= "<p>".$this->single_video_page_dd($post, $id)."</p>";
        $metabox_html .= "<p>".$this->single_video_tagname_dd($post, $id.'_tag')."</p>";

        echo $metabox_html;
    }


        static function pauple_video_nav(){
          $post_id = get_the_ID();
          $num_of_videos = 3;

          $nav_html = "<div class='fbva1-section2'><ul class='container-wrap'>";

          for($ii = 1; $ii <= 3; $ii++ ){
              $video[$ii] = array(
                  'video_meta' => get_post_meta($post_id, 'videopress_video'.$ii, true),
                  'video_tag_meta' => get_post_meta($post_id, 'videopress_video'.$ii.'_tag', true)
              );
              $nav_html .= self::get_single_nav($video[$ii]['video_meta'], $video[$ii]['video_tag_meta']);

          }
          $nav_html .= "</div></ul</div>";



          echo $nav_html;

        }

        public function get_single_nav($video1_id, $video1_tag){
          $args = array(
            'post_type' =>'pauple_videopress',
            'post_status' => 'publish',
            'id' => $video1_id,
          );


          $video1 = get_post($video1_id);

          $title = $video1->post_title;
          $content = $video1->post_content;
          $permalink = get_permalink();
          $thumbnail_url = get_the_post_thumbnail_url($video1_id);
          $nav_html = self::pauple_video_nav_html($title, $content,  $permalink, $thumbnail_url, $video1_tag);
          wp_reset_query();
          return $nav_html;
        }

        public function pauple_video_nav_html($title, $content, $permalink, $thumbnail_url, $video1_tag){
            echo "video_tag: ".$video1_tag . " , ";
          $nav_html =  '';
          $nav_html .= "<li class='fbva1-section2-video'> ".do_shortcode( "[show_if has_tag='".$video1_tag."']"."<span class='video-permission-active'>"."[/show_if]");
          $nav_html .= do_shortcode( "[show_if has_tag='".$video1_tag."']"."<a href='".$permalink."'>"."[/show_if]");
          $nav_html .= "<div class='fbva1-video-link-wrap'>".$has_permission;
          $nav_html .= "<div class='fbva1-video-link-overlay'>Now Playing</div>";
          $nav_html .= "<img src='".$thumbnail_url."' />";
          $nav_html .= "</div>";
          $nav_html .= "<h4>".$content."</h4>";
          $nav_html .= do_shortcode( "[show_if has_tag='".$video1_tag."']"."</a></span>"."[/show_if]");
          $nav_html .= "</li>";

          return $nav_html;

        }

    }

  ?>
