<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (!class_exists('PAUPLE_VIDEOPRESS_CPT')) :
    class PAUPLE_VIDEOPRESS_CPT
    {
        public $post_type_name = 'pauple_videopress';

        public function helpie_load_wp_media_files() {
          wp_enqueue_media();
        }

        public function __construct()
        {

            add_action('init', array($this, 'register_element'));
            add_action( 'admin_enqueue_scripts', array ( $this, 'helpie_load_wp_media_files'));
        }

        public function register_taxonomies()
        {
            // Add new taxonomy, make it hierarchical (like categories)

            $labels = array(
                'name' => _x('VideoPress Categories', 'taxonomy general name', 'pauple-videopress'),
                'singular_name' => _x('VideoPress Category', 'taxonomy singular name', 'pauple-videopress'),
                'search_items' => __('Search VideoPress Categories', 'pauple-videopress'),
                'all_items' => __('All VideoPress Categories', 'pauple-videopress'),
                'parent_item' => __('Parent VideoPress Category', 'pauple-videopress'),
                'parent_item_colon' => __('Parent VideoPress Category:', 'pauple-videopress'),
                'edit_item' => __('Edit VideoPress Category', 'pauple-videopress'),
                'update_item' => __('Update VideoPress Category', 'pauple-videopress'),
                'add_new_item' => __('Add New VideoPress Category', 'pauple-videopress'),
                'new_item_name' => __('New VideoPress Category Name', 'pauple-videopress'),
                'menu_name' => __('VideoPress Category', 'pauple-videopress'),
            );

            $args = array(
              'hierarchical' => true,
              'labels' => $labels,
              'show_ui' => true,
              'show_admin_column' => true,
              'query_var' => true,
              'rewrite' => array('slug' => 'videopress_category'),
            );

            register_taxonomy('videopress_category', array('pauple_videopress'), $args);
        }

        public function register_element()
        {

            $labels = array(
                'name' => 'VideoPress Article',
                'singular_name' => 'VideoPress Article',
                'add_new' => 'Add New VideoPress Article',
                'add_new_item' => 'Add New VideoPress Article',
                'edit' => 'Edit',
                'edit_item' => 'Edit VideoPress Article',
                'new_item' => 'New VideoPress Article',
                'view_item' => 'View VideoPress Article',
                'search_items' => 'Search VideoPress Articles',
                'not_found' => 'No VideoPress Articles found',
                'parent' => 'Parent VideoPress Articles',
                'filter_items_list' => 'Filter elements list',
                'items_list' => 'VideoPress Articles list',
                'items_list_navigation' => 'Elements list navigation',
                'menu_name' => __('VideoPress', 'pauple-videopress'),
            );



            $args = array(
                'labels' => $labels,
                'public' => true,
                'menu_position' => 30.1,
                'menu_icon' => 'dashicons-schedule',
                'show_in_nav_menus' => true,
                'map_meta_cap' => true,
                'supports' => array('title', 'editor', 'excerpt', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'post-formats', 'thumbnail'),
                'has_archive' => true,
                'rewrite' => array('slug' => 'videopress'),
            );

            register_post_type($this->post_type_name, $args);
            $this->register_taxonomies();
            flush_rewrite_rules();
        }






    } // END CLASS
endif;

$cpt = new PAUPLE_VIDEOPRESS_CPT();
