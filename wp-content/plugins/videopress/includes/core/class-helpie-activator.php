<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Fired during plugin activation.
 *
 * @link       http://pauple.com
 * @since      1.0.0
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 *
 * @author     Your Name <email@example.com>
 */
class PAUPLE_HELPIE_ACTIVATOR
{
    /**
     * Short Description. (use period).
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {


        $cpt = new PAUPLE_HELPIE_CPT();
        $cpt->register_element();

        // Create Pages
        include_once('create-pages.php');
        pauple_helpie_wc_create_page('helpdesk_search', 'helpdesk_search_page_id', 'Helpdesk Search');

        self::store_default_settings();

        flush_rewrite_rules();
    }

    public static function store_default_settings(){
        $option1 = array(
            'kb_main_title' => 'Helpdesk',
            'helpie_basic_user_access' => 'anyone'
        );
        update_option('helpie_core_options_main', $option1);

        $roles = get_editable_roles();
        foreach ($roles as $role_key => $value) {
             $option2 = array(
                 0 => 'all',
             );
             $option_name = $role_key.'_allowed_helpie_terms';
            update_option($option_name, $option2);
        }

        $option3 = array(
            'main_page_categories' => 'on',
            'main_page_popular' => 'on',
            'helpie_breadcrumbs' => 'on',
            'helpie_sidebar_type' => 'full-nav',
            'helpie_cat_page_sidebar_display' => 'on',
            'helpie_cat_page_search_display' => 'on',
            'helpie_cat_page_widget_recent' => 'on',
            'helpie_single_page_sidebar_display' => 'on',
            'helpie_single_page_search_display' => 'on',
        );

        update_option('helpie_components_options', $option3);

        $mp_options = array(
            'helpie_mp_template' => 'boxed1',
            'main_page_search_display' => 'on',
        );
        update_option('helpie_mp_options', $mp_options);

    }
}
