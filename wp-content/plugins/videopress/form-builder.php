<?php

class PAUPLE_VIDEOPRESS_FORM_BUILDER{

    public function get_dropdown($id, $saved_value, $options){

        $dd_html = '';
        $dd_html .= "<label for='".$id."'>Video Post Title: </label>";
        $dd_html .= "<select name='".$id."' id='".$id."'>";

        foreach ($options as $key => $value) {
            if( isset($saved_value) && $key == $saved_value){
            $dd_html .= "<option selected='selected' value='".$key."'>".$value."</option>";
            }else{
            $dd_html .= "<option value='".$key."'>".$value."</option>";
            }
        }
        $dd_html .= "</select>";

        return $dd_html;
    }

} // End of Class

?>
