<?php

include_once dirname(dirname(__FILE__)).'/models/customer-model.php';

if (!class_exists('LS_COURSES_SINGLE_COURSE')) {
    class LS_COURSES_SINGLE_COURSE
    {
        private $customer;

        public function __construct()
        {
            $this->customer = new LS_COURSES_CUSTOMER_MODEL();
        }

        public function render_navigation()
        {
            echo $this->get_navigation();
        }

        public function render_content()
        {
            echo '<ul class = "content-list"> ';
            for ($x = 1; $x <= 100; ++$x) {
                $content = 'content'.$x;
                $title = 'title'.$x;
                $courseIntro = 'intro'.$x;

                if (get_post_meta(get_the_ID(), $content, true) != '') {
                    echo '<li class = "module-content-'.$x.'">';

                    echo '<h3 class = "module-2016-intro-title"><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-film-icon.png" /><span>'.get_post_meta(get_the_ID(), $title, true).'</span></h3><div class = "content-inner">'.get_post_meta(get_the_ID(), $content, true).'</div></li>';
                }
            }
            echo '</ul>';
        }

        public function get_navigation()
        {
            $course_id = get_post_meta(get_the_ID(), 'course_id', true);
            $html = '';
            $html .= '<div class = "module-list">';

            if (get_post_meta(get_the_ID(), 'courseintro', true) != '') {
                $html .= '<div class = "course-2016-module-intro"><h3><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-overview-bubble.png" /><span>Welcome</span></h3>'.get_post_meta(get_the_ID(), 'courseintro', true).'</div>';
            }

            $html .= '<ul><h3><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-modules-icon.png" /><span>Course Modules</span></h3> ';
            $overview = 'overview';
            if (get_post_meta(get_the_ID(), $overview, true) != '') {
                $html .= '<div class = "course-2016-overview"><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-calendar-icon.png" /><p>'.get_post_meta(get_the_ID(), $overview, true).'</p></div>';
            }

            $post_meta = get_post_meta(get_the_ID());
            $titles = array();
            $contents = array();
            foreach ($post_meta as $key => $value) {
                if (preg_match('/^title/', $key)) {
                    $titles[$key] = $value;
                }

                if (preg_match('/^content/', $key)) {
                    $contents[$key] = $value;
                }
            }

            $count = 1;
            $customer_modules = $this->customer->get_course_modules($course_id);

            foreach ($titles as $key => $value) {
                $active_class = '';
                $current_module_number = str_replace('title', '', $key);
                if (in_array($current_module_number, $customer_modules)) {
                    $active_class = 'module-active-link';
                }
                $html .= "<li><a class='module-title module-title-".$count.' '.$active_class."'> ".$value[0].'</a></li>';
                ++$count;
            }

            $html .= '</ul>';
            $html .= '</div>';

            return $html;
        }
    } // END CLASS
}
