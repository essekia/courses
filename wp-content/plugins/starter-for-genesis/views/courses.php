<?php


include_once dirname(dirname(__FILE__)).'/models/customer-model.php';
class LS_COURSES_COURSE_LIST
{
    private $customer;

    public function __construct()
    {
        $this->customer = new LS_COURSES_CUSTOMER_MODEL();
    }

    public function nice_var_dump($data)
    {
        echo '<pre>';
        print_r($data);
        //var_dump($data);
        echo '</pre>';
    }

    public function get_single_course()
    {   
         $this->customer->send_reset_email();

         // var_dump(wp_get_current_user());
        $courses = array(
            0 => array(
                'img' => 'https://smartmarketer.com/wp-content/uploads/tmba.jpg',
                'title' => 'Learn to create a winning sales funnel using Facebook video ads.',
                'content' => '<p>Facebook Video Ads Mastery is a 5 week, in-depth training where Ezra teaches you the same ads strategy he used to grow his business into one of the largest Shopify stores in the world.</p>',
                'course_link' => 'http://69.195.124.100/~vpnsolut/courses/traffic-mba/',
                'info_link' => 'google.com',
                'footer_link_label' => 'Get Started',
                'course_id' => 'Traffic',
            ),
            1 => array(
                'img' => 'https://community.smartmarketer.com/wp-content/uploads/2016/04/ocu-banner.jpg',
                'title' => 'Ignite Course',
                'content' => '<p>Ignite is a 4 month program that is really good for you.</p>',
                'course_link' => 'http://community.liyanasilver.com/ignite/',
                'info_link' => 'google.com',
                'course_id' => 'Ignite',
            ),
            2 => array(
                'img' => 'https://community.smartmarketer.com/wp-content/uploads/2015/12/oss-logo.png',
                'title' => 'Manwhispering Course',
                'content' => '<p>Manswhispering is the course you need, there is no way that’s easier or more cost-effective than The One Stop Shop training along with our Proven Profitable Theme.</p>',
                'course_link' => 'http://community.liyanasilver.com/manwhispering/',
                'info_link' => 'google.com',
                'course_id' => 'Manwhispering',
            ),

            3 => array(
                'img' => 'https://community.smartmarketer.com/wp-content/uploads/2015/12/content-logo.png',
                'title' => 'Learn how to create and market engaging content in order to build, scale and grow your business.',
                'content' => "<p>Originally released for the Shopify eCommerce University, Content Marketing for Traffic &amp; Sales teaches you the most powerful thing you can be doing to grow your business.</p><p>You'll learn about content creation, syndication, and marketing, and how the whole process impacts sales.</p>",
                'course_link' => 'http://community.smartmarketer.com/content-marketing-for-traffic-and-sales/',
                                'info_link' => 'google.com',
            ),
            4 => array(
                'img' => 'https://community.smartmarketer.com/wp-content/uploads/2015/12/conv-course.png',
                'title' => 'Make sure you’re getting the most sales from your traffic!',
                'content' => "<p>This course is geared specifically towards eCommerce business owners who want to increase their conversions with Conversion Rate Optimization.</p>
<p>This is over 7 years worth of Ezra's split testing data compiled into 7 videos, teaching you how to get the most value out of your traffic. We share with you what has proven to work best for the header, footer, landing pages, videos and more.</p>",
                'course_link' => 'http://community.smartmarketer.com/ecommerce-conversion-course/',
                                'info_link' => 'google.com',
                'footer_link_label' => 'Get Started',
            ),
            5 => array(
                'img' => 'https://smartmarketer.com/wp-content/uploads/SM_CoursePage_OPP_LOGO.png',
                'title' => 'This is the most proven method for getting started selling on Amazon.',
                'content' => '<p>
From sourcing your products, to creating an optimized product listing, to learning how to boost your search ranking by advertising on Facebook and Instagram…</p><p>
Operation Physical Products teaches you everything you need to know to be a successful seller on Amazon, taken straight from Ezra’s years of experience creating multiple 6- and 7-figure Amazon businesses. </p>',
                'course_link' => 'http://operationphysicalproducts.com/',
                                'info_link' => 'google.com',
                'footer_link_label' => 'More Info',
            ),

        );

        $html = "<ul class='community-page-breadcrumbs' id='community-page-breadcrumbs'>";
        $html .= "<li><a href='http://community.smartmarketer.com'><img src='http://community.smartmarketer.com/wp-content/uploads/2016/02/home-breadcrumb-icon.png'></a></li>"; 
        $html .= "<li><a href='http://community.smartmarketer.com/event-recordings/'><img src='http://community.smartmarketer.com/wp-content/uploads/2016/04/courses-breadcrumb.png'></a></li>";
        $html .= '</ul>';
        $html .= "<div class='courses-list'><ul>";

        //echo sizeof($courses);
        for ($ii = 0; $ii < sizeof($courses); ++$ii) {
            //echo $ii . "- ";
            //echo "</br>";

            $html .= "<li><div class='sm-courses-image'>";
            $html .= "<img src='".$courses[$ii]['img']."' data-pin-nopin='true'>";
            $html .= '</div>';
            $html .= "<div class='sm-course-description'><div class='course-description-content'>";
            $html .= '<p><strong>'.$courses[$ii]['title'].'</strong></p> ';
            $html .= $courses[$ii]['content'];

            $html .= '</div></div>';
            $html .= "<div class='course-footer-links'>";
            if (isset($courses[$ii]['course_id']) && !empty($courses[$ii]['course_id']) && $this->customer->is_course_available_to_current_user($courses[$ii]['course_id'])) {
                $html .= "<a class='sm-course-link' href='".$courses[$ii]['course_id']."'>GET STARTED</a>";
            } else {
                $html .= "<a class='sm-course-link course-link-no-access' href='".$courses[$ii]['info_link']."'>MORE INFO</a>";
            }

            $html .= '</div>';
            $html .= '</li>';
        }
        $html .= '</ul></div>';

        return $html;
    }
}
