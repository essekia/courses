<?php
		
class Forget_Password
  {

	public function render_forget_passwd()
		{
			// $html  = "<div class='site-inner'>";
			// $html .= "<div class='wrap'>";
			$html = "<div class='content-sidebar-wrap'>";
            $html .= "<main class='content1'>";
			$html .= "<article class='post-416 page type-page status-publish entry' itemscope='' itemtype='https://schema.org/CreativeWork'>";
			$html .= "<header class='entry-header'>";
			$html .= "<h1 class='entry-title1' itemprop='headline'>Forgot Password</h1>";
			$html .= "</header>";
			$html .= "<div class='entry-content1' itemprop='text'>";
			$html .= "<p class='left-align'>Forgot password? Fill in your email address below to have your password send to you by email.</p>";
			$html .= "<p class='left-align'><strong>NOTE:</strong> If you receive an email with a blank password try using a different email address or contact support@liyanasilver.com.</p>";
			echo $html;
		}

	public function render_forget_passwd_1()
	   {

			$html  = "<form action='".get_site_url()."/mail-send' method='post' name='memb_password_send-1' id='memb_password_send-1' >";
			$html .= "<input type='hidden' name='form_id' value='1'>";
			$html .= "<input type='hidden' name='template_id' value='0'><input type='hidden' name='signature' value='2a6556fb9e7c9065d62fc934dab36ce3650f0b1734790104e2df8996ea1444d8'>";
			$html .= "<input type='hidden' name='successurl' value=''>";
			$html .= "<input type='hidden' name='failureurl' value=''>";
			$html .= "<input type='hidden' name='memb_form_type' value='memb_send_password'>";
			$html .= "<div id='memb_password_send-1-block1'>";
			$html .= "<label id='memb_password_send-1-email-label'>Email Address:</label>";
			$html .= "<input id='memb_password_send-1-email-input' name='email' type='email' required='' value=''>";
			$html .= "</div>";
			$html .= "<div id='memb_password_send-1-block2'>";
			$html .= "<input id= 'submit1' type='submit' value='Reset Password' name='submit'></div>";
			$html .= "</form>";
			$html .= "</div>";
			$html .= "</article>";
			$html .= "</main>";
			$html .= "</div></div></div>"; 
			echo $html;
		}


	}
?>