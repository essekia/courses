<?php
class Member_Login{


public function render_login_wrap() {
    
            
            $html  = "<div class='content-sidebar-wrap'>";
            $html .= "<main class='content'>";
            $html .= "<article class='post-2760 page type-page status-publish entry' itemscope='' itemtype='https://schema.org/CreativeWork'>";
            $html .= "<header class='entry-header'>
                            <h1 class='entry-title' itemprop='headline'>Member Login</h1>
                 </header>";

            $html .= "<div class='entry-content' itemprop='text'>";
            $html .= "<style>@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');</style>";

            return $html;
        }    

public function render_login_form() {  
            $html = "<div class='member-login-section'>"; 
            $html .= " <div class='member-login-wrap'>";
            $html .= "<h2><img src='".get_site_url()."/wp-content/uploads/2017/03/welcome-back.jpg'>
                        </h2>";
            $html .= "<div style='clear:both;'></div>";      

            $html .= "<form name='loginform' id='loginform' action='http://community.smartmarketer.com/wp-login.php' method='post'>";
                    
            $html .= "<p class='login-username'>";
            $html .= "<label for='user_login'> email </label>";
            $html .= "<input type='text' name='log' id='user_login' class='input' value='' size='20'>";
            $html .= "</p>";

            $html .= "<p class='login-password'>";
            $html .= "<label for='user_pass'> password </label>";
            $html .= "<input type='password' name='pwd' id='user_pass' class='input' value='' size='20'>";
            $html .= "</p>";
                    
            $html .= "<p class='login-remember'>";
            $html .= "<label><input name='rememberme' type='checkbox' id='rememberme' value='forever'> Remember Me </label>";
            $html .= "</p>";

            $html .= "<p class='login-submit'>";
            $html .= "<input type='submit' name='wp-submit' id='wp-submit' class='button button-primary' value='Log In'>";
            $html .= "<input type='hidden' name='redirect_to' value=''>";
            $html .= "</p>";

             $html .= "</form>";
             return $html;

         }

public function render_passwd_reset_section() {   

            $html = "<div style='clear:both;'></div>";
            $html .= "<div class='password-reset-section'>
                Forgot password? <a href='".get_site_url()."/forgot-password/'>Click here to reset.</a>
                </div>";
            $html .= "</div>";
            return $html;
        }

public function render_member_login_aside_video() { 

            $html = "<div class='member-login-aside'>";
            $html .= "<a href='https://pages.smartmarketer.com/3-part-facebook-video-advertising-mastery-watch-video-1/'>";
            $html .= "<img src='http://courses.dev/wp-content/uploads/2017/03/signout-banner.jpg'>";
            $html .= "</a>";
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</article>";
            $html .= "</main>";
            $html .= "</div></div></div>";

          return $html;

        }



    public function render_footer_widget() {


        $html  = "<div class='footer-widgets'> <div class='wrap'>";
        $html .= "<div class='widget-area footer-widgets-1 footer-widget-area' style='height: 229px;'>";
        $html .= "<section id='ra_social-2' class='widget widget-1 widget widget-first widget-full widget_ra_social'>";
        $html .= "<div class='widget-wrap'>";
        $html .= "<h4 class='widget-title widgettitle'> Follow Us </h4>";
        $html .= "<ul class='socialmedia'><li class='facebook'> <!--<span class='genericon genericon-facebook-alt'></span>--><img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/facebook-icon.jpg'><a target='_blank' href='http://www.facebook.com/meetezra'>facebook</a></li><li class='pinterest'> <!--<span class='genericon genericon-pinterest'></span>--><img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/pinterest-icon.jpg'><a target='_blank' href='http://www.pinterest.com/ezrafirestone/'>pinterest</a></li><li class='twitter'> <!--<span class='genericon genericon-twitter'></span>--> <img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/twitter-icon.jpg'><a target='_blank' href='https://twitter.com/ezrafirestone'>twitter</a></li><li class='gplus'> <!--<span class='genericon genericon-googleplus-alt'></span>--> <img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/gplus-icon.jpg'><a target='_blank' href='https://plus.google.com/+EzraFirestone'>google plus</a></li><li class='linkedin'> <!--<span class='genericon genericon-linkedin'></span>--> <img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/linkedin-icon.jpg'><a target='_blank' href='https://www.linkedin.com/in/ezrafirestone'>linkedin</a></li><li class='youtube'> <!--<span class='genericon genericon-youtube'></span>--><img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/youtube-icon.jpg'><a target='_blank' href='https://www.youtube.com/user/TheSmartMarketer'>youtube</a></li>
                    </ul>";
        $html .= "</div>";
        $html .= "</section>";
        $html .= "</div>";
        return $html;
    }



public function render_footer_widget_2() {
        $html = "<div class='widget-area footer-widgets-2 footer-widget-area' style='height: 229px;'>";
        $html .= "<section id='text-14' class='widget widget-1 widget widget-first widget-full widget_text'>";
        $html .= "<div class='widget-wrap'>";
        $html .= "<div class='textwidget'>";
        $html .= "<ul>";
        $html .= "<li><a href='http://community.smartmarketer.com/'>Community Home</a></li>";
        $html .= "<li><a href='http://community.smartmarketer.com/ecommerce-courses/'>eCommerce Courses</a></li>";
        $html .= "<li><a href='http://community.smartmarketer.com/account'>My Account</a></li>";
        $html .= "</ul>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</section>";
        $html .= "</div>";
        return $html;
    }

public function render_footer_widget_3() {
        $html = "<div class='widget-area footer-widgets-3 footer-widget-area' style='height: 229px;'>";
        $html .= "<section id='ra_image-2' class='widget widget-1 widget widget-first widget-half widget_ra_image'>";
        $html .= "<div class='widget-wrap'>";
        $html .= "<div class='imagewidget'>";
        $html .= "<a href='http://www.SmartMarketer.com'>
                    <img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/community-sm-logo.jpg' class='alignnone'>
                    </a>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</section>";
        $html .= "<section id='text-7' class='widget widget-2 widget widget-last widget-half widget_text'>";
        $html .= "<div class='widget-wrap'>"; 
        $html .= "<div class='textwidget'>";
        $html .= "<p><strong>Smart Marketer Inc.</strong><br>
                    982 Main Street, Suite 4-315<br>
                    Fishkill, NY 12524<br>
                    Phone: 800-770-8216</p>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</section>";
        $html .= "</div>";
        $html .= "</div></div>";

            return $html;

        }

      

}

   // $LOGIN_VIEW_obj= new Memberlogin();
   // $LOGIN_VIEW_obj->render_login();


?>




