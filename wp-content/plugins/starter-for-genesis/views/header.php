<?php

class LS_COURSES_HEADER
{
    public function __construct()
    {
        add_action('genesis_after_header', array($this, 'render_all_header_sections'));
        add_action('genesis_before_header', array($this, 'before_header_hooks'));
    }

    public function before_header_hooks()
    {   

    }

    public function render_all_header_sections()
    {   
        echo $this->get_community_util_nav();
        echo $this->get_community_brand_area();
        $this->get_header();
    }

    public function get_community_util_nav()
    {
        $html = "<div class='community-utility-nav'><div class='wrap'>";

        $html .= "<section id='text-12' class='widget widget-1 widget widget-first widget-full widget_text'>";
        $html .= "<div class='widget-wrap'>	";
        $html .= "<div class='textwidget'>";
        // $html .= "<a target='_blank' class='community-utility-smcom' href='http://www.smartmarketer.com'>Visit SmartMarketer.com</a>";

        if( is_user_logged_in() ){
            $html .= "<a href='".wp_logout_url( $redirect )."' title='Logout' class='memb_logout_link'>Log Out</a>";
        }
        else {
             $html .= "<a href='".get_site_url()."/member-login' title='Login' class='memb_logout_link'>Log In</a>";
        }
        $html .= "</div></div></section></div></div>";

        return $html;
    }

    public function get_community_brand_area()
    {
        $html = "<div class='community-header-area'><div class='wrap'>";
        $html .= "<section id='ra_image-54' class='widget widget-1 widget widget-first widget-full widget_ra_image'>";
        $html .= "<div class='widget-wrap'><div class='imagewidget'><a href='http://community.smartmarketer.com'>";
        $html .= "<img src='http://community.smartmarketer.com/wp-content/uploads/2017/03/community-logo.png' class='alignnone' data-pin-nopin='true'>";
        $html .= "</a></div></div>";
        $html .= "</section></div></div>";
        return $html;
    }


    public function get_header()
    {
        $nav_items = array(
            0 => array(
                'label' => 'Community Home',
                'classes' => 'nav-menu-home nav-active',
                // 'url' => 'http://community.smartmarketer.com'
                'url' => get_site_url()."/member-login"
            ),
            1 => array(
                'label' => 'My Courses',
                'classes' => 'nav-menu-courses nav-active',
                'url' => get_site_url()."/courses"
            ),
            2 => array(
                'label' => "What's Working Now Recodings",
                'classes' => 'nav-menu-working nav-inactive',
                'url' => 'http://community.smartmarketer.com/event-recordings/'
            ),
            3 => array(
                'label' => "My Account",
                'classes' => 'nav-menu-account nav-active',
                'url' => 'http://community.smartmarketer.com/whats-working-now/'
                // 'url' => get_site_url()."/whats-working-now/"
            ),
            4 => array(
                'label' => 'Facebook Group',
                'classes' => 'nav-menu-facebook nav-inactive',
                'url' => 'http://community.smartmarketer.com/blue-ribbon-mastermind/'
            ),
        );
        $html  = "<div class='community-nav-menu community-main-nav' id='community-main-nav'>";
        $html .= "<div class='wrap'>";
        $html .= "<section id='text-13' class='widget widget-1 widget widget-first widget-full widget_text'>";
        $html .= "<div class='widget-wrap'>";
        $html .= "<div class='textwidget'><div class='menu-community-menu-container'><ul id='menu-community-menu' class='menu'> ";

        for ($ii = 0; $ii < sizeof($nav_items); $ii++) {
            $html .= "<li class='menu-item ".$nav_items[$ii]['classes']."'> <!----><a href='".$nav_items[$ii]['url']."' itemprop='url'>".$nav_items[$ii]['label']."</a> </li>";
        }

        $html .= "</ul> </div></div></div></section> </div></div>";
        echo $html;
    }
}
