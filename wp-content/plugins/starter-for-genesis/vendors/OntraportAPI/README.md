# ONTRAPORT PHP library

## Requirements
PHP version 5.3.2 or greater.

## Documentation
Visit our [API documentation](https://api.ontraport.com/doc) page for detailed usage information and code examples.

## Composer
If you are using composer, you can install our SDK on the command line by entering
```
composer require ontraport/sdk-php
```
You can also manually add it to your `composer.json` file:
```
{
    "require": {
        "ontraport/sdk-php": "*"
    }
}
```
To use the library, include Composer's autoload in your scripts:
```
require_once('vendor/autoload.php');
```
## Manual Installation
Download or clone the latest version of this repo. Make sure to include the Ontraport.php file to use this library in your code:
```
require_once('path/to/src/Ontraport.php');
```
