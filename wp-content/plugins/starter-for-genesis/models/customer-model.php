<?php
// namespace ventors\OntraportAPI\src;
// require_once ('Ontraport.php');
// use OntraportAPI\Ontraport;
include_once dirname(dirname(__FILE__)).'/env.php';

require_once STARTER_GENESIS_PATH.'vendors/OntraportAPI/src/Ontraport.php';
use OntraportAPI\Ontraport;


if (!class_exists('LS_COURSES_CUSTOMER_MODEL')) {
    class LS_COURSES_CUSTOMER_MODEL
    {
        private $pilotpress;
        private $user_email;
        // private $user_passwd;
        private $app_id;
        private $api_key;
        private $site_url;

        public function __construct()
        {
            error_log('LS_COURSES_CUSTOMER_MODEL');
            $current_user = wp_get_current_user();
            $this->user_email = $current_user->user_email;
                 // $this->user_passwd = $current_user->user_passwd;
            $env = new LS_COURSES_ENV();
            $env_info = $env->get_env();
            $this->app_id = $env_info['app_id'];
            $this->api_key = $env_info['api_key'];
            $this->site_url = $env_info['site_url'];

            add_action('wp_ajax_ls_courses_login', array($this, 'ls_courses_login'));
            add_action('wp_ajax_nopriv_ls_courses_login', array($this, 'ls_courses_login'));
        }

        public function get_this_domain(){
            $domain = get_site_url();
            $domain = str_replace("http://","", $domain);
            $domain = str_replace("https://","", $domain);
            return $domain;
        }
        public function make_api_call($url, $headers)
        {
            /* Refer: https://github.com/Ontraport/WooCommerceIntegration/blob/master/include/Ontraport.php */
            $chscan = curl_init();
            curl_setopt($chscan, CURLOPT_URL, $url);
            curl_setopt($chscan, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($chscan, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($chscan, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chscan, CURLOPT_SSL_VERIFYPEER, false);
            $order_res = curl_exec($chscan);
            return $order_res;
   
        }

        public function create_user_session_wp($email){

                // Automatic login //
                $user = get_user_by( 'email', $email );

                // Redirect URL //
                if ( !is_wp_error( $user ) )
                {
                    wp_clear_auth_cookie();
                    wp_set_current_user ( $user->ID );
                    wp_set_auth_cookie  ( $user->ID );

                    // $redirect_to = user_admin_url();
                    // wp_safe_redirect( $redirect_to );
                    // exit();
                }
        }

        public function create_user_wp($email, $password){
            error_log(' email does not exists');
            $username = $email ;

            $create_user = wp_create_user( $username, $password, $email );
            // error_log(' create_user: ' . $create_user );
        }

        public function create_user_in_wp_if_absent( $email, $password ){
            
            error_log('create_user_in_wp_if_absent');

            if ( !email_exists( $email ) ) {
                // Create user
                $this->create_user_wp($email, $password);
                $this->create_user_session_wp($email);
            } else{
                // Login to session - start user session
                $this->create_user_session_wp($email);
            }
        }


        public function ls_courses_login(){
            error_log('ls_courses_login');

            if( isset($_POST['email']) && !empty($_POST['email'])){
                $email = sanitize_text_field($_POST['email']);
            }

            if( isset($_POST['password']) && !empty($_POST['password'])){
                $password = sanitize_text_field($_POST['password']);
            }

            $response = $this->check_user_password($email, $password);

            if( $response == 'Correct Password'){
                $this->create_user_in_wp_if_absent( $email, $password);
            }

            // error_log("username: " . $username);
            // error_log(" , password: " . $password);


            echo $response;

            wp_die();
        }

        public function reset_password($email, $given_password){

            if( isset($email) && !empty($email)){
                $contact_id = $this->get_contact_id($email);

                $client = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX"); 
                $requestParams = array(
                    "id"        => $contact_id,
                    "f1437" => $given_password, // password field
                );

                $response = json_decode($client->contact()->update($requestParams));

                // echo "<pre>";
                // var_dump( $response->data->attrs);
                // echo "</pre>";

                $status = 'Failed: Password not updated';

                if( isset($response->data) && isset($response->data->attrs) 
                    && isset($response->data->attrs) && !empty($response->data->attrs->f1437)){
                    $saved_password =  $response->data->attrs->f1437;
                    // echo "saved_password: " . $saved_password;

                    if($saved_password == $given_password){
                        $status = 'Success: Password successfully updated';
                    }

                }
   
                return $status;
            }


        }

        public function check_user_password($email, $given_password)
        {   
            error_log("check_user_password");
            error_log("email: " . $email);
            error_log("given_password: " . $given_password);
            $headers = array(
                  'Accept: application/json',
                  'Content-Type: application/json',
                  'Api-Appid: '.$this->app_id,
                  'Api-Key: '.$this->api_key,
            );


            $client = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX");   

            $requestParams = array(
                "condition" => 
                    "[{
                        \"field\":{\"field\":\"email\"},
                        \"op\":\"=\",
                        \"value\":{\"value\":\"$email\"}
                    }]",
            );

            $response = $client->contact()->retrieveMultiple($requestParams);
            $response = json_decode($response);

            $data = $response->data;
            $password = $data[0]->f1437;
            // echo $password;

            error_log("correct password: " . $password);
            if( isset($password) && !empty($password) && ($given_password == $password) ){
                return "Correct Password";
            } else{
                return "Wrong Password";
            }

        }


        public function send_reset_email($email = "catchepaul@gmail.com", $given_password = "alpha09")
        {   
            $headers = array(
                  'Accept: application/json',
                  'Content-Type: application/json',
                  'Api-Appid: '.$this->app_id,
                  'Api-Key: '.$this->api_key,
            );


            $client = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX");   

            $requestParams = array(
                "condition" => 
                    "[{
                        \"field\":{\"field\":\"email\"},
                        \"op\":\"=\",
                        \"value\":{\"value\":\"$email\"}
                    }]",
            );

            $response = $client->contact()->retrieveMultiple($requestParams);

            $response = json_decode($response);
            $data = $response->data;

            if( isset($data ) && !empty($data )){
                return  "Email exists.";
            } else{
                return "Email does not exist.";
            }
        }

        public function ontraport_api_get_contact($email, $option){
            // $headers = array(
            //       'Accept: application/json',
            //       'Content-Type: application/json',
            //       'Api-Appid: '.$this->app_id,
            //       'Api-Key: '.$this->api_key,
            // );


            $client = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX");   

            $requestParams = array(
                "condition" => 
                    "[{
                        \"field\":{\"field\":\"email\"},
                        \"op\":\"=\",
                        \"value\":{\"value\":\"$email\"}
                    }]",
            );
            
            if($option == 'meta'){
                 $response = $client->contact()->retrieveMeta($requestParams);  
            } else{
                $response = $client->contact()->retrieveMultiple($requestParams);
            }

            return $response;
        }


        public function get_contact_id($email){
            $contact_info = $this->ontraport_api_get_contact($email,$option);
            $contact_info = json_decode($contact_info);

            $data = $contact_info->data;
            // echo "<pre>";
            // var_dump( $contact_info);
            // echo "</pre>";

            $contact_id = $data[0]->id;
            return $contact_id;
        }

        public function get_membership_of_contact($email){

            $contact_id = $this->get_contact_id($email);
            $domain = $this->get_this_domain();
         
            $client = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX");    
                $requestParams = array(
                    "objectID" => 43, // WordPress site object
                     "condition" => 
                    "[{
                        \"field\":{\"field\":\"contactemail\"},
                        \"op\":\"=\",
                        \"value\":{\"value\":\"$email\"}
                    },
                    \"AND\",
                    {
                        \"field\":{\"field\":\"url\"},
                        \"op\":\"=\",
                        \"value\":{\"value\":\"$domain\"}
                    }]",
                );
           
            $response = $client->object()->retrieveMultiple($requestParams);
            $response = json_decode($response);

            $membership_levels = $response->data[0]->membership_level;
            
            // echo "<pre>";
            // var_dump( $membership_levels);
            // echo "</pre>";

            return $membership_levels;
        }

        public function get_site_id(){
            // echo "get_site_id: ";

            $domain = $this->get_this_domain();
            // echo "domain: " . $domain;

            $site = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX");    
                $requestParams = array(
                    "objectID" => 67, // WordPress site object
                    "condition" => 
                    "[{
                        \"field\":{\"field\":\"domain\"},
                        \"op\":\"=\",
                        \"value\":{\"value\":\"$domain\"}
                    }]",
                );

            $response = $site->object()->retrieveMultiple($requestParams);

            $response = json_decode($response);

            $data = $response->data[0];

            // echo "<pre>";
            // var_dump( $data );
            // echo "</pre>";

            $site_id = $data->website_id;
            // echo "site_id : " .$site_id;
            return $site_id;
        }

        public function get_current_site_membership($email)
        {   

            error_log('get_current_site_membership');

            // $this->get_contact_id($email);
            // $this->get_membership_of_contact($email);
            

              $client = new Ontraport("2_101315_45GiJSlx5","S1VgP5T2OiPeiGX");    
                $requestParams = array(
                    "objectID" => 67, // WordPress site object
                    "id"       => 0
                );

            $response = $client->object()->retrieveMultiple($requestParams);

            $response = json_decode($response);
            $user_data = $response->data;



            $user_current_site_membership = '';
            for ($ii = 0; $ii < sizeof($user_data); ++$ii) {
                $site_obj = $user_data[$ii];
                // echo "domain: " . $this->get_this_domain();
                if ($site_obj->website_url == $this->get_this_domain() ) {
                    $user_current_site_membership = json_decode($site_obj->membership_levels);
                }
            }
            
            // echo "user_current_site_membership: ";
            // print_r($user_current_site_membership);
            return $user_current_site_membership;
        }

        public function get_contact_by_email()
        {
            $headers = array(
                'Accept: application/json',
                'Content-Type: application/json',
                'Api-Appid: 2_101315_45GiJSlx5',
                'Api-Key: S1VgP5T2OiPeiGX',
            );

            $params = array(
                 'objectID' => 0,
                 'condition' => "username='".$this->user_email."'",
            );

            $params = http_build_query($params);
            $post_url = 'https://api.ontraport.com/1/objects?'.$params;
            $order_res = $this->make_api_call($post_url, $headers);

            return $order_res;
        }

        public function is_course_available_to_current_user($course_id)
        {
            $is_available = false;
            $current_user_membership = $this->get_membership_of_contact($this->user_email);
         
            if (isset($current_user_membership) && !empty($current_user_membership)) {
                for ($ii = 0; $ii < sizeof($current_user_membership); ++$ii) {
                    $subject = $current_user_membership[$ii];
                    $pattern = '/'.$course_id.'/i';
                    // echo "course_id: " . $course_id;
                    // echo "<br>subject: " .$subject;
                    if (preg_match($pattern, $subject)) {
                        $is_available = true;
                    }
                }
            }

            return $is_available;

        }


        public function get_course_modules($course_id)
        {
            $course_modules = array();
            $current_user_membership = $this->get_current_site_membership($this->user_email);

            $count = 0;

            if (isset($current_user_membership) && !empty($current_user_membership)) {
                for ($ii = 0; $ii < sizeof($current_user_membership); ++$ii) {
                    $subject = $current_user_membership[$ii];
                    $pattern = '/'.$course_id.'/';
                    $pattern_to_remove = $course_id.'_Module';
                    if (preg_match($pattern, $subject)) {
                        $course_modules[$count] = str_replace($pattern_to_remove, '', $subject);
                        ++$count;
                    }
                }
            }

            return $course_modules;
        }

   

    } // END CLASS
}


