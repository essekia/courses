<?php
/**
 * Template Name: Content Delivery
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

add_filter('body_class', 'ra_content_class');
function ra_content_class($classes) {
	$classes[] = 'content-delivery';
	return $classes;
}







global $content;


add_action( 'genesis_before_entry_content', 'ra_content_delivery_carousel', 11 );
function ra_content_delivery_carousel() {

	$foo = get_queried_object();
	$parent = $foo->post_parent;
	// print_r( $parent );
	$args = array(
		'post_type' => 'page',
		'posts_per_page' => -1,
		'post_parent' => $parent,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);
	
	
	
	$directoryURI = $_SERVER['REQUEST_URI'];
	$path = parse_url( $directoryURI, PHP_URL_PATH );
	$components = explode('/', $path);
	// var_dump($components);
	$first_part = $components[3];

	$slides = get_posts( $args );
	echo '<div class="course-carousel">';
	foreach ( $slides as $slide ) : setup_postdata( $slide );
		$id = get_post_thumbnail_id( $slide->ID );
		$image = wp_get_attachment_image_src( $id, 'full' );
		// var_dump( $slide );
		$image = $image[0];
		$image = aq_resize( $image, 134, 72, true );
		$slug = $slide->post_name;
		$video = get_post_meta($post->ID, '_ra_content_video', true);
		if ( $first_part == $slug ){
			$class = 'active';
		} else {
			$class= 'inactive';
		}
		
		echo '<div class="item course '.$class.'">';
			echo '<a class="link" href="'.get_permalink( $slide->ID ).'"><img src="'.$image.'" alt="'.get_the_title( $slide->ID ).'"/></a>';
			
		echo '</div>';
		
		
		
		
		



				
		
		
	endforeach;

	echo '</div>';



	
}










add_action('genesis_before_entry_content', 'ra_content_delivery_content', 12);

function ra_content_delivery_content() {
	global $post;

	$foo = get_queried_object();

	$subheadline = get_post_meta($post->ID, '_ra_content_subheadline', true);
	//$progress = get_post_meta($post->ID, '_ra_content_progress', true);
	//echo $progress;
	$video = get_post_meta($post->ID, '_ra_content_video', true);
	$url = get_post_meta($post->ID, '_ra_content_url', true);
	$button_text = get_post_meta($post->ID, '_ra_content_button_text', true);
	$cbback = get_post_meta($post->ID, '_ra_content_cbback', true);
	$cbtext = get_post_meta($post->ID, '_ra_content_cbtext', true);
	$cburl = get_post_meta($post->ID, '_ra_content_cburl', true);
	$all = get_post_meta( $post->ID, '_ra_content_all', true );
	$previous = get_post_meta( $post->ID, '_ra_content_previous', true );
	$next = get_post_meta( $post->ID, '_ra_content_next', true );
	$content = get_post_field('post_content', $post->ID);
	echo '<div class="progressbar">';
		echo '<div></div>';
		//echo '<div class="clear"></div>';
	echo '</div>';
	echo '<div class="subheadline">';
		echo '<small>'.$subheadline.'</small>';
	echo '</div>';
	echo '<div class="clear"></div>';
	echo '<div class="video">';
		echo (!empty($video) ? $video : '');
		

	echo '</div>';
	echo '<div class="clear"></div>';
	/*echo '<div class="button-link">';
		echo (!empty($url) ? '<a class="more-link" target="_blank" href="'.$url.'">'.(!empty($button_text) ? $button_text : 'Yes, I\'m Ready').'</a>' : '');
	echo '</div>';
	if ($cbback == 'on') {
		echo '<div class="back-button">';
			echo (!empty($cburl) ? '<a class="back-link" href="'.$cburl.'">'.(!empty($cbtext) ? $cbtext : 'Back To All Videos In This Course').'</a>' : '');
		echo '</div>';



	}*/
	echo '<div class="button-links">';
		echo !$all ? '' : '<span class="all"><a href="'.$all.'"><span class="icon">All Videos</span></a></span>';
		echo !$previous ? '' : '<span class="previous"><a href="'.$previous.'"><span class="icon">Previous Video</span></a></span>';
		echo !$next ? '' : '<span class="next"><a href="'.$next.'"><span class="icon">Next Video</span></a></span>';
	echo '</div>';

echo $content;

}





add_action('wp_footer', 'ra_content_script', 99);
function ra_content_script() { ?>
	<script>
		(function($){
			<?php global $post; $progress = get_post_meta($post->ID, '_ra_content_progress', true); ?>
			/*function progress(percent, $element) {
				var progressBarWidth = percent * $element.width() / 100;
				$element.find('div').animate({ width: progressBarWidth }, 500).html('');
			}
			progress(<?php echo $progress; ?>, $('.progressbar'));*/
			$('.course-carousel').owlCarousel({
				items: 6,
				itemsDesktopSmall: [940,5],
				itemsTablet: [768,4],
				itemsMobile: [604,3],
				pagination: false,
				navigation: true
			});
		})(jQuery);
	</script>
<?php }

genesis();