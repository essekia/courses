<?php
/**
 * Template Name: Mastermind Page Template
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

add_filter('body_class', 'ra_mastermind_class');
function ra_mastermind_class($classes) {
	$classes[] = 'mastermind';
	return $classes;
}

remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_entry_content', 'ra_mastermind_entry');

function ra_mastermind_entry() {
	global $post;
	$subheadline = get_post_meta($post->ID, '_ra_mastermind_subheadline', true);
	$video = get_post_meta($post->ID, '_ra_mastermind_video', true);
	$url = get_post_meta($post->ID, '_ra_mastermind_url', true);

	echo (!empty($subheadline) ? '<h2 class="subheadline">'.$subheadline.'</h2>' : '');
	echo (!empty($video) ? '<div class="video">'.$video.'</div>' : '');
	echo (!empty($url) ? '<div class="button-link"><a class="more-link" target="_blank" href="'.$url.'">Yes, I’m Ready</a></div>' : '');
}

genesis();