<?php

/**
 * Template Name:  Reset Action Page
 *
 * Page Used: courses
 * URL : http://courses.dev/reset-login/
 */
 get_header();

// echo "Reset Action Page";
 if (isset($_POST['submit']))
  {
  	// echo "submit isset.";
	$new_password = sanitize_text_field($_REQUEST['passwd1']);
	
	$retype_password = sanitize_text_field($_REQUEST['passwd2']);

	$email = $_GET['email'];

	if( $new_password == $retype_password ){
		include_once dirname(dirname(dirname(__FILE__))).'/models/customer-model.php';
		$customer_model = new LS_COURSES_CUSTOMER_MODEL();
		echo $customer_model->reset_password($email, $new_password);
		// echo "Password successfully reset.";
	} else {
		echo "Passwords did not match.";
	}
} 

?>
