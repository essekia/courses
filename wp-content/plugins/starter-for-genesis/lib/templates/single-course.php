<?php
/**
 * Template Name: Single Course
 *
 * A template for displaying a list of Smart Marketer courses
 *
 * @since 	1.0.0
 *
 * @version	1.0.0
 */
 
// genesis();



add_action('genesis_entry_content', 'display_single_course', 1);
function display_single_course()
{

    // echo "heelllo";
    include_once dirname(dirname(dirname(__FILE__))).'/views/single-course.php';
 	$single_course = new LS_COURSES_SINGLE_COURSE();
    echo $single_course->render_navigation();
}

genesis();