<?php
/**
 * Template Name: Community Category
 *
 * A template for displaying community content courses
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */


function ra_comm_category_entry() {
	

	echo (!empty($subheadline) ? '<div class ="content-page-inner"><div class ="">
	
	<h2 class="subheadline">'.$subheadline.'</h2>' : '');
	echo (!empty($video) ? '<div class="video">'.$video.'</div>' : '');
	echo (!empty($url) ? '<div class="button-link"><a class="more-link" target="_blank" href="'.$url.'">Yes, I’m Ready</a></div>' : '');

}




//outputs page intro from custom field data
add_action('genesis_before_content', 'display_page_intro');


function display_page_intro(){
	global $post;
	$subheadline = get_post_meta($post->ID, '_ra_comm_category_subheadline', true);
	$content = get_post_meta($post->ID, '_ra_comm_category_content', true);
	$url = get_post_meta($post->ID, '_ra_comm_category_button_url', true);
	$button = get_post_meta($post->ID, '_ra_comm_category_button_text', true);
	$cta = get_post_meta($post->ID, '_ra_comm_category_cta', true);
	
	if ($content != ''){
	echo '<div class="entry-header"><div class = "category-heading-left">';
	echo (!empty($subheadline) ? '<h2 class="subheadline">'.$subheadline.'</h2>' : '');
	echo (!empty($content) ? '<p>'.$content.'</p>' : '');
	echo '</div>';
	echo (!empty($cta) ? '<div class = "category-cta"><p>'.$cta.'</p>' : '');
	echo (!empty($content) ? '<a href = "'.$url.'">'.$button.'</a>' : '');
	echo '</div></div>';
	
	?>
	<script>
	window.onload = function(){
	
	var title = document.getElementsByClassName('entry-header');
	title[1].className="entry-header no-display";
	}
	</script>
	<?php
	
}}

add_action( 'genesis_meta', 'ra_primary_meta' );
function ra_primary_meta() {
	if ( is_active_sidebar( 'sidebar' )  )
		add_action( 'genesis_after_entry_content', 'ra_primary_sidebar', 5 );
}



function ra_primary_sidebar() {
	if ( is_active_sidebar( 'sidebar' ) )
		echo '<div class="primary-link-list">';
			dynamic_sidebar( 'sidebar' );
		echo '</div>';
}


//this outputs the custom field data, loops through each imageX and linkX
add_action('genesis_after_entry_content', 'display_image_links');
function display_image_links() {
	echo '<ul class = "category-link-list"> ';
		$x = 1; 
		$continue = true;
	
		while($continue == true) {
			$image = 'image' . $x;
			$link = 'link' . $x;
			if (get_post_meta(get_the_ID(), $image, TRUE) != ''){
			echo '<li><a href = "'.get_post_meta(get_the_ID(), $link, TRUE).'"><img src = "'.get_post_meta(get_the_ID(), $image, TRUE).'" /></a></li>';}
			$x++;
			$image = 'image' . $x;
			if (get_post_meta(get_the_ID(), $image, TRUE) == ''){
				$continue = false;	
			} 
		}
	echo '</ul></div>';
}

add_action('genesis_after_entry_content', 'display_member_content');
function display_member_content() {
	if (memb_hasAnyTags("302"))
		{echo "<div>".get_post_meta(get_the_ID(), "community-member", TRUE)."</div>";}
	elseif (memb_hasAnyTags("294"))
		{echo "<div>".get_post_meta(get_the_ID(), "pincommerce-member", TRUE)."</div>";}
	elseif (memb_hasAnyTags("340"))
		{echo "<div>".get_post_meta(get_the_ID(), "bootcamp-member", TRUE)."</div>";}
	elseif (memb_hasAnyTags("310"))
		{echo "<div>".get_post_meta(get_the_ID(), "blueribbon-member", TRUE)."</div>";}
	elseif (memb_hasAnyTags("318"))
		{echo "<div>".get_post_meta(get_the_ID(), "blueribbon-elite-member", TRUE)."</div>";}
}


genesis();