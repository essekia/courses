<?php
/**
 * Template Name: Course Overview
 *
 * A template for displaying Pincommerce course
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

/*
//outputs page intro from custom field data
add_action('genesis_before_loop', 'display_page_intro');
function display_page_intro(){
	echo '<div class = "content-page-inner"><div class = "content-page-intro">';
	echo get_post_meta(get_the_ID(), 'intro', TRUE);
	echo '</div>';
}
*/

$access1 = get_post_meta(get_the_ID(), 'accesstag1', TRUE);
$accessTag1 = (int) $access1;
$access2 = get_post_meta(get_the_ID(), 'accesstag2', TRUE);
$accessTag2 = (int) $access2;
$access3 = get_post_meta(get_the_ID(), 'accesstag3', TRUE);
$accessTag3 = (int) $access3;
$access4 = get_post_meta(get_the_ID(), 'accesstag4', TRUE);
$accessTag4 = (int) $access4;
$access5 = get_post_meta(get_the_ID(), 'accesstag5', TRUE);
$accessTag5 = (int) $access5;
$access6 = get_post_meta(get_the_ID(), 'accesstag6', TRUE);
$accessTag6 = (int) $access6;
$access7 = get_post_meta(get_the_ID(), 'accesstag7', TRUE);
$accessTag7 = (int) $access7;
$access8 = get_post_meta(get_the_ID(), 'accesstag8', TRUE);
$accessTag8 = (int) $access8;
$access9 = get_post_meta(get_the_ID(), 'accesstag9', TRUE);
$accessTag9 = (int) $access9;


if (get_post_meta(get_the_ID(), 'accesstag1', TRUE) != ''){
    if (memb_hasAnyTags($accessTag1) OR memb_hasAnyTags($accessTag2) OR memb_hasAnyTags($accessTag3) OR memb_hasAnyTags($accessTag4) OR memb_hasAnyTags($accessTag5) OR memb_hasAnyTags($accessTag6) OR memb_hasAnyTags($accessTag7) OR memb_hasAnyTags($accessTag8) OR memb_hasAnyTags($accessTag9)) {}
    else{
     header("Location: http://community.smartmarketer.com");
        exit();
    }
}



//this outputs the custom field data, loops through each titleX
add_action('genesis_after_entry_content', 'display_custom_fields', 1);
function display_custom_fields() {
    // include_once dirname(dirname(dirname(__FILE__))).'/views/header.php';
    // $header = new LS_COURSES_HEADER();
    // echo $header->get_header();
	echo '<div class = "module-list">';

            if (get_post_meta(get_the_ID(), 'courseintro', TRUE) != ''){

    echo '<div class = "course-2016-module-intro"><h3><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-overview-bubble.png" /><span>Welcome</span></h3>'.get_post_meta(get_the_ID(), "courseintro", TRUE).'</div>';  }


    echo '<ul><h3><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-modules-icon.png" /><span>Course Modules</span></h3> ';
    $overview = 'overview';
    if (get_post_meta(get_the_ID(), $overview, TRUE) != ''){echo '<div class = "course-2016-overview"><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-calendar-icon.png" /><p>'.get_post_meta(get_the_ID(), $overview, TRUE).'</p></div>';}
	for ($x = 1; $x <= 100; $x++){
		$title = 'title' . $x;
        $content = 'content' . $x;
		if (get_post_meta(get_the_ID(), $title, TRUE) != ''){
			echo '<li><a class = "module-title module-title-'.$x.'';
            if (get_post_meta(get_the_ID(), $content, TRUE) != ''){ echo ' module-active-link ';}
            echo '"> '.get_post_meta(get_the_ID(), $title, TRUE).'</a></li>';
			}
		}
	echo '</ul>';
	echo '</div>';
}

//this outputs the custom field data, loops through each contentX
add_action('genesis_after_entry_content', 'display_custom_fields2', 2);
function display_custom_fields2() {
	echo '<ul class = "content-list"> ';
		for ($x = 1; $x <= 100; $x++){
		$content = 'content' . $x;
		$title = 'title' . $x;
        $courseIntro = 'intro' .$x;


		if (get_post_meta(get_the_ID(), $content, TRUE) != ''){
			echo '<li class = "module-content-'.$x.'">';

             echo '<h3 class = "module-2016-intro-title"><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/03/comm-film-icon.png" /><span>'.get_post_meta(get_the_ID(), $title, TRUE).'</span></h3><div class = "content-inner">'.get_post_meta(get_the_ID(), $content, TRUE).'</div></li>';

			}
		}
	echo '</ul>';
}





add_action( 'genesis_meta', 'ra_primary_meta' );
function ra_primary_meta() {
	if ( is_active_sidebar( 'sidebar' )  )
		add_action( 'genesis_after_entry_content', 'ra_primary_sidebar' );
}

function ra_primary_sidebar() {
	if ( is_active_sidebar('sidebar') )
		echo '<div class="primary-link-list">';
			dynamic_sidebar( 'sidebar' );
		echo '</div>';
}


genesis();
