<?php
/**
 * Template Name: Live Event
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

remove_action('genesis_footer', 'ra_do_footer');
remove_action('genesis_after_header', 'genesis_do_nav');
//unregister_sidebar('header-right');
remove_theme_support('genesis-footer-widgets');

add_filter('body_class', 'ra_live_event_class');
function ra_live_event_class($classes) {
	$classes[] = 'live-event';
	return $classes;
}

add_action('genesis_after_header', 'ra_hidden_div');
function ra_hidden_div() {
	echo '<div id="hidden" class="hidden">';
		echo ' ';
	echo '</div>';
}

remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_entry_content', 'ra_live_event_entry');

function ra_live_event_entry() {
	global $post;
	$subheadline = get_post_meta($post->ID, '_ra_live_event_subheadline', true);
	$video = get_post_meta($post->ID, '_ra_live_event_video', true);
	$text = get_post_meta($post->ID, '_ra_live_event_button_text', true);
	$url = get_post_meta($post->ID, '_ra_live_event_url', true);

	echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');
	echo (!empty($video) ? '<div class="video">'.$video.'</div>' : '');
	echo (!empty($url) ? '<div class="button-link"><a class="more-link" target="_blank" href="'.$url.'">'.(!empty($text) ? $text : 'Get My Tickets').'</a></div>' : '');
}

add_action('genesis_before_footer', 'ra_live_event_settings', 2);

function ra_live_event_settings() {
	do_action('ra_live_event_settings');
}

add_action('ra_live_event_settings', 'ra_do_something', 11);
function ra_do_something() {
	echo '<div class="hidden clearfix"></div>';
}

add_action('ra_live_event_settings', 'ra_do_speakers', 12);
function ra_do_speakers() {
	global $post;
	$headline = get_post_meta($post->ID, '_ra_event_speakers_headline', true);
	$subheadline = get_post_meta($post->ID, '_ra_event_speakers_subheadline', true);
	genesis_markup(array(
		'html5' => '<div %s id="speakers">',
		'xhtml' => '<div class="speakers" id="speakers">',
		'context' => 'speakers'
	));

	echo genesis_structural_wrap('speakers', 'open', 0);
			
			echo (!empty($headline) ? '<h2 class="headline">'.$headline.'</h2>' : '');
			echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');
			
			if (is_active_sidebar('speakers'))
				echo '<div class="speaker-sidebar">';
					dynamic_sidebar('speakers');
				echo '</div>';
	
	echo genesis_structural_wrap('speakers', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_agenda', 13);
function ra_do_agenda() {
	global $post;
	$headline = get_post_meta($post->ID, '_ra_event_agenda_headline', true);
	$subheadline = get_post_meta($post->ID, '_ra_event_agenda_subheadline', true);

	genesis_markup(array(
		'html5' => '<div %s id="schedule">',
		'xhtml' => '<div class="agenda" id="agenda">',
		'context' => 'agenda'
	));

	echo genesis_structural_wrap('agenda', 'open', 0);
		echo (!empty($headline) ? '<h2 class="headline">'.$headline.'</h2>' : '');
		echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');
		if (is_active_sidebar('agenda'))
			echo '<div class="agenda-widget">';
				dynamic_sidebar('agenda');	
			echo '</div>';
	echo genesis_structural_wrap('agenda', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_workshops', 14);
function ra_do_workshops() {
	global $post;
	$headline = get_post_meta($post->ID, '_ra_event_workshop_headline', true);
	$subheadline = get_post_meta($post->ID, '_ra_event_workshop_subheadline', true);

	genesis_markup(array(
		'html5' => '<div %s id="workshops">',
		'xhtml' => '<div class="workshops" id="workshops">',
		'context' => 'workshops'
	));

	echo genesis_structural_wrap('workshops', 'open', 0);
		echo (!empty($headline) ? '<h2 class="headline">'.$headline.'</h2>' : '');
		echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');
		if (is_active_sidebar('workshop'))
			echo '<div class="workshop-widget">';	
				dynamic_sidebar('workshop');
			echo '</div>';	
	echo genesis_structural_wrap('workshops', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_tickets', 15);
function ra_do_tickets() {
	genesis_markup(array(
		'html5' => '<div %s id="tickets">',
		'xhtml' => '<div class="tickets" id="tickets">',
		'context' => 'tickets'
	));

	echo genesis_structural_wrap('tickets', 'open', 0);
		if (is_active_sidebar('tickets'))
			dynamic_sidebar('tickets');
	echo genesis_structural_wrap('tickets', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_travel_accommodation', 16);
function ra_do_travel_accommodation() {
	genesis_markup(array(
		'html5' => '<div %s id="travel">',
		'xhtml' => '<div class="travel-accommodation" id="travel-accommodation">',
		'context' => 'travel-accommodation'
	));

	echo genesis_structural_wrap('travel-accommodation', 'open', 0);
		global $post;
		$headline = get_post_meta($post->ID, '_ra_event_travel_headline', true);
		$subheadline = get_post_meta($post->ID, '_ra_event_travel_subheadline', true);
		echo (!empty($headline) ? '<h2 class="headline">'.$headline.'</h2>' : '');
		echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');

		if (is_active_sidebar('travel-accommodation'))
			echo '<div class="travel-accommodation-widget">';
				dynamic_sidebar('travel-accommodation');
			echo '</div>';	
	echo genesis_structural_wrap('travel-accommodation', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_testimonials', 17);
function ra_do_testimonials() {
	global $post;
	$headline = get_post_meta($post->ID, '_ra_event_testimonial_headline', true);
	$subheadline = get_post_meta($post->ID, '_ra_event_testimonial_subheadline', true);

	genesis_markup(array(
		'html5' => '<div %s id="testimonials">',
		'xhtml' => '<div class="testimonials" id="testimonials">',
		'context' => 'testimonials'
	));

	echo genesis_structural_wrap('testimonials', 'open', 0);

		echo (!empty($headline) ? '<h2 class="headline">'.$headline.'</h2>' : '');
		echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');
		if (is_active_sidebar('testimonial'))
			echo '<div class="testimonial-widget">';
				dynamic_sidebar('testimonial');
			echo '</div>';
	echo genesis_structural_wrap('testimonials', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_faq', 18);
function ra_do_faq() {
	global $post;
	$headline = get_post_meta($post->ID, '_ra_event_faq_headline', true);
	$subheadline = get_post_meta($post->ID, '_ra_event_faq_subheadline', true);

	genesis_markup(array(
		'html5' => '<div %s id="faq">',
		'xhtml' => '<div class="faq" id="faq">',
		'context' => 'faq'
	));

	echo genesis_structural_wrap('faq', 'open', 0);
		echo (!empty($headline) ? '<h2 class="headline">'.$headline.'</h2>' : '');
		echo (!empty($subheadline) ? '<p class="subheadline">'.$subheadline.'</p>' : '');
		if (is_active_sidebar('faq'))
			echo '<div class="faq-widget">';
				dynamic_sidebar('faq');
			echo '</div>';
	
	echo genesis_structural_wrap('faq', 'close', 0);

	echo '</div>';
}

add_action('ra_live_event_settings', 'ra_do_tickets_2', 19);
function ra_do_tickets_2() {
	genesis_markup(array(
		'html5' => '<div %s id="tickets-2">',
		'xhtml' => '<div class="tickets" id="tickets-2">',
		'context' => 'tickets'
	));

	echo genesis_structural_wrap('tickets', 'open', 0);
		if (is_active_sidebar('tickets'))
			dynamic_sidebar('tickets');
	echo genesis_structural_wrap('tickets', 'close', 0);

	echo '</div>';
}

add_action('wp_footer', 'ra_live_event_meta', 2);
function ra_live_event_meta() {
	wp_enqueue_script('onepage.js');
	// wp_enqueue_script('single.js');
	?>
	<script>
		jQuery(function($){
			var stick = $('.site-header').offset().top;
			var marg = $('.site-header').height();

		    var sticky = function(){
		    var scrollT = $(window).scrollTop();

		    if (scrollT > stick) {
		        $('.site-header').addClass('sticky');
		        $('.wrap').addClass('addmargin');
		        //$('.addmargin').css('margin-top', marg);
		    } else {
		        $('.site-header').removeClass('sticky');
		        $('.wrap').removeClass('addmargin');
		        //$('.wrap').css('margin-top', 0);
		    }
		    };

		    sticky();

		    $(window).scroll(function() {
		        sticky();
		    });

		    $(window).scroll(function(){
		        if ($(this).scrollTop() > 100) {
		            $('.gototop').fadeIn();
		        } else {
		            $('.gototop').fadeOut();
		        }
		    });

		    $('.gototop').click(function(){
		        $("html, body").animate({ scrollTop: 0 }, 600);
		        return false;
		    });

		    $('#menu-live-event').onePageNav();
		    /*$('#menu-live-event').singlePageNav({
		    	offset: $('#menu-live-event').outerHeight(),
		    });*/
		});
	</script>
	<?php
}

genesis();