<?php
/**
 * Template Name: About Page Template
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

add_filter('body_class', 'ra_about_class');
function ra_about_class($classes) {
	$classes[] = 'about';
	return $classes;
}

remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_entry_content', 'ra_about_content');

function ra_about_content() {
	global $post;
	echo '<div class="video one-half first">';
		$video = get_post_meta($post->ID, '_ra_about_video', true);
		echo $video;
	echo '</div>';
	echo '<div class="about-content one-half">';
		echo the_content();
	echo '</div>';
}

genesis();