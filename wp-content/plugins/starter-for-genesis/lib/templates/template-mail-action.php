<?php
/**
 * Template Name: mail-send-action Page Template
 *
 * Page Used: courses
 * URL : http://courses.dev/mail-send/
 */
get_header();
?>
<link rel="stylesheet" type="text/css" href="../wp-content/plugins/starter-for-genesis/lib/css/style.css" >
  <?php

   

    if (isset($_POST['submit']))  {

        
        $email =  sanitize_email($_REQUEST['email']);
        $subject = ' New message Received ';
        $activation_url = get_site_url()."/reset-login/";
        $key = 'activation_key';
        $activation_key = md5(mt_rand(100, 1000));

        $secure_url = add_query_arg( 'activation_key', $activation_key, $activation_url );
        $secure_url = add_query_arg( 'email', $email , $secure_url );

        
        $transient_key = $email.'_ontralogin';


        // Save the API response so we don't have to call again until tomorrow.
        set_transient( $transient_key, $activation_key, DAY_IN_SECONDS );

        $fetch = get_transient($transient_key);
        // echo "get_transient: ";
        // print_r($fetch);
   

 // Always leave empty space in the first character
        $activation_link = '<a href= "'.$secure_url.'" title="Verify"> '.$activation_url.' </a>';
        $message = '<p> Reset the password Verification Link: ' . $activation_link . ' </p>';

        //Send the email
        add_filter('wp_mail_content_type', create_function('', 'return "text/html";'));
        
        error_log("email: " . $email);
        error_log("message: " . $message);
        error_log("subject: " . $subject);

        $mail_result = wp_mail( $email, $subject, $message);
       
        //Email response
         echo "Reset mail link has been sent, kindly check your mail box...";
         echo "</br>";
         echo "Thank you for contacting us!";
    }

    // $path = dirname(dirname(dirname(__FILE__)))."/views/mail-send-action.php";
    //  include_once $path;
    //  $mail_obj = new Mail_Send(); 
   
     // echo $mail_obj ->render_mail_send();
     
     // echo $mail_obj->render_reset_login_1();

     // include_once dirname(dirname(dirname(__FILE__)))."/views/member-login.php";
     //  $member_obj = new Member_Login();
     // echo $member_obj->render_footer_widget(); 
     // echo $member_obj->render_footer_widget_2(); 
     // echo $member_obj->render_footer_widget_3(); 

?> 


<?php get_footer(); ?>