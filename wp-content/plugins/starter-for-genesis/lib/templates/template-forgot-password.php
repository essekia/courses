<?php
/**
 * Template Name:  forgot-password Page Template
 *
 * Page Used: courses
 * URL : http://courses.dev/forgot-password/
 */
get_header();
?>
<link rel="stylesheet" type="text/css" href="../wp-content/plugins/starter-for-genesis/lib/css/style.css" >
  <?php
    $path = dirname(dirname(dirname(__FILE__)))."/views/forgot-password.php";
     include_once $path;
     $Forget = new Forget_Password(); 
   
     echo $Forget->render_forget_passwd();
     
     echo $Forget->render_forget_passwd_1();

     include_once dirname(dirname(dirname(__FILE__)))."/views/member-login.php";
      $member_obj = new Member_Login();
     echo $member_obj->render_footer_widget(); 
     echo $member_obj->render_footer_widget_2(); 
     echo $member_obj->render_footer_widget_3(); 

?>


<?php get_footer(); ?>