 <?php
/**
 * Template Name:  login Page Template
 *
 * Page Used: courses
 * URL : http://courses.dev/member-login/
 */
get_header();

        $customer_model_dir = dirname(dirname(plugin_dir_path(__FILE__))).'/models/customer-model.php';
        include_once $customer_model_dir;
        $customer_model = new LS_COURSES_CUSTOMER_MODEL();

?>

  <link rel="stylesheet" type="text/css" href="../wp-content/plugins/starter-for-genesis/lib/css/style.css" >
<?php

$login_js_url = plugins_url().'/starter-for-genesis/lib/js/login.js';

function login_scripts() {
    $login_script_nonce = wp_create_nonce( 'login_script_nonce' );

     wp_enqueue_script( 'login-script',  plugins_url( '../js/login.js' , __FILE__ ) , array( 'jquery' ), '1.0.0', true);
     wp_localize_script('login-script', 'my_ajax_object', array('ajax_url' => admin_url('admin-ajax.php')));
     wp_localize_script( 'login-script', 'loginScriptNonce', $login_script_nonce );
     wp_localize_script( 'login-script', 'pluginsDirURL', plugins_url() );
     wp_localize_script( 'login-script', 'siteURL', get_site_url() );



}

add_action( 'wp_enqueue_scripts', 'login_scripts' );
    
    // echo $login_js_url;

    $path = dirname(dirname(dirname(__FILE__)))."/views/member-login.php";
    include_once $path;
    $login_view = new Member_Login();
    echo $login_view->render_login_wrap();
    
    if( is_user_logged_in() ){
        // echo $login_view->render_logout_form();
    } else{
        
        echo $login_view->render_login_form();
        echo $login_view->render_passwd_reset_section();
    }

    
    echo $login_view->render_member_login_aside_video();
    echo $login_view->render_footer_widget();
    echo $login_view->render_footer_widget_2();
    echo $login_view->render_footer_widget_3();
 ?>
 
<!-- </body>
</html> -->

<?php get_footer(); ?>