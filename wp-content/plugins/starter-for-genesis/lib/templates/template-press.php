<?php
/**
 * Template Name: Press Page Template
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

add_filter('body_class', 'ra_press_class');
function ra_press_class($classes) {
	$classes[] = 'press';
	return $classes;
}

//remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_after_entry_content', 'ra_press_entry');

function ra_press_entry() {
	$args = array(
		'orderby' => 'name',
		'order' => 'DESC',
		'hide_empty' => false,
	);

	$terms = get_terms('interview_category', $args);
	$count = count($terms); $i = 0;
	//var_dump($terms);
	if ($count > 0) {
		foreach ($terms as $term) {
			//var_dump($term);
			$id = $term->term_id;
			$tax = $term->taxonomy;
			$press = get_cuztom_term_meta($id, $tax);
			echo '<div class="press-entry '.$term->slug.'">';
				$query = array(
					'posts_per_page' => -1,
					'post_type' => 'interview',
					'tax_query' => array(
						array(
							'taxonomy' => $tax,
							'field' => 'slug',
							'terms' => $term->slug
						)
					)
				);

				$loop = new WP_Query($query);
				$counter = 1;
				if ($loop->have_posts()) :
					echo '<h2 class="entry-title">'.$term->name.'</h2>';
					while ($loop->have_posts()) : $loop->the_post();
						if ($counter % 4 == 1) {
							$class = 'one-fourth first';
						} else {
							$class = 'one-fourth';
						}

						$image = ra_post_image();
						if (!empty($image)) {
							$image = aq_resize($image, 220, 180, true);
						} else {
							$image = 'http://placehold.it/220x180';
						}

						echo '<div class="press-list '.$class.'">';
							echo '<a href="'.get_the_content().'" target="_blank"><img src="'.$image.'" alt="'.get_the_title().'"/></a>';
							echo '<h4 class="press-title"><a href="'.get_the_content().'" target="_blank">'.get_the_title().'</a></h4>';
						echo '</div>';
						$counter++;
					endwhile;
					wp_reset_postdata();
				endif;
			echo '</div>';
			$i++;
		}
	}
}

add_action( 'genesis_meta', 'ra_press_meta' );
function ra_press_meta() {
	if ( is_active_sidebar( 'press-page-1' ) || is_active_sidebar( 'press-page-2' ) || is_active_sidebar( 'press-page-3' ) || is_active_sidebar( 'press-page-4' ) )
		add_action( 'genesis_after_entry_content', 'ra_press_sidebar' );
}

function ra_press_sidebar() {
	if ( is_active_sidebar( 'press-page-1' ) )
		echo '<div class="press-page-1">';
			dynamic_sidebar( 'press-page-1' );
		echo '</div>';

	if ( is_active_sidebar( 'press-page-2' ) )
		echo '<div class="press-page-2">';
			dynamic_sidebar( 'press-page-2' );
		echo '</div>';

	if ( is_active_sidebar( 'press-page-3' ) )
		echo '<div class="press-page-3">';
			dynamic_sidebar( 'press-page-3' );
		echo '</div>';

	if ( is_active_sidebar( 'press-page-4' ) )
		echo '<div class="press-page-4">';
			dynamic_sidebar( 'press-page-4' );
		echo '</div>';
}

add_action( 'wp_footer', 'ra_press_script' );
function ra_press_script() { ?>
	<script type="text/javascript">
		jQuery(function($){
			$('.press-page-2 .widget').addClass('one-fourth');
			$('.press-page-2 .widget').each(function(a,b){
				if (a%4 == 0){
					$(b).addClass('first');
				}
				// var title = $('.widget_ra_image .widgettitle').detach();
				// title.appendTo('.imagewidget');

			});
			// $('.widget_ra_image .widgettitle')each().detach().appendTo('.widget_ra_image .imagewidget');
			$('.widget_ra_image .widgettitle').each(function(){
				// $(this).detach().appendTo('.imagewidget');
			});

			$('.widget_ra_image').each(function(){
				$('.widget_ra_image .widgettitle').each(function(){
					// $(this).detach().appendTo('.imagewidget');
				});
			});
		});
	</script>
<?php }

genesis();