<?php
/**
 * Template Name: Individual Courses
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

add_filter('body_class', 'ra_individual_class');
function ra_individual_class($classes) {
	$classes[] = 'individual-courses';
	return $classes;
}

add_action('genesis_after_entry_content', 'ra_sharre_plugin');
function ra_sharre_plugin() {
	global $post;
	echo '<div id="share-course">';
		echo '<div class="shareme" data-url="'.get_permalink().'" data-text="'.get_the_title().'"></div>';
	echo '</div>';
}

add_action('genesis_after_loop', 'ra_individual_videos');
function ra_individual_videos() {
	global $post;
	$foo = get_queried_object();

	$args = array(
		'post_type' => 'page',
		'posts_per_page' => -1,
		'post_parent' => $foo->ID,
		'orderby' => 'menu_order',
		'order' => 'ASC'
	);

	$pages = get_posts($args);
	//var_dump($pages);
	$count = 1;
	echo '<div class="course-list">';
		foreach ($pages as $page) : setup_postdata($page);
			$image = wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'large');
			$image = $image[0];
			if (!empty($image)) {
				$image = aq_resize($image, 300, 169, true);
			} else {
				$image = 'http://placehold.it/300x169';
			}
			//var_dump($image);
			//echo $image[0];

			if ($count % 3 == 1) {
				$class = 'one-third first';
			} else {
				$class = 'one-third';
			}

			echo '<div class="video '.$class.'">';
				echo '<a href="'.get_permalink($page->ID).'"><img alt="'.$page->post_title.'" src="'.$image.'"/></a>';
				echo '<h4 class="post-title"><a href="'.get_permalink($page->ID).'">'.$page->post_title.'</a></h4>';
			echo '</div>';
			$count++;
		endforeach;
		wp_reset_postdata();
	echo '</div>';
}

add_action('wp_footer', 'ra_sharre_script');
function ra_sharre_script() {
	wp_register_script(
		'jquery-sharrre',
		RA_PLUGIN_URL . 'lib/js/jquery.sharrre.min.js',
		array('jquery'),
		time(),
		true
	);

	wp_enqueue_script('jquery-sharrre');
}

add_action('wp_footer', 'ra_sharre_do', 100);
function ra_sharre_do() { ?>
	<script>
		(function($){
			$('.shareme').sharrre({
				share: {
					googlePlus: true,
					facebook: true,
					twitter: true,
					linkedin: true,
				},
				buttons: {
					googlePlus: {size: 'tall', annotation:'bubble'},
					facebook: {layout: 'box_count'},
					twitter: {count: 'vertical'},
					linkedin: {counter: 'top'},
				},
				enableHover: false,
				enableCounter: false,
				enableTracking: true
			});	
		})(jQuery);
		</script>
<?php }

genesis();