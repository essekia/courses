<?php
/**
 * Template Name: reset-action-validate Page Template
 *
 * Page Used: courses
 * URL : http://courses.dev/reset-action/
 */
get_header();
?>

<?php
include_once dirname(dirname(dirname(__FILE__)))."/lib/actions/read-action.php";
 
     $reset_action_obj = new  Reset_action();
     echo $reset_action_obj->render_reset_action(); 
?>

<?php get_footer(); ?>