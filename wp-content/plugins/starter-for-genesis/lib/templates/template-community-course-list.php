<?php
/**
 * Template Name: Community Course List
 *
 * A template for displaying a list of Smart Marketer courses
 *
 * @since 	1.0.0
 *
 * @version	1.0.0
 */
 include_once dirname(dirname(dirname(__FILE__))).'/views/courses.php';
 $courses = new LS_COURSES_COURSE_LIST();

add_action('genesis_after_entry_content', 'display_courses_fields', 1);
function display_courses_fields()
{
    global $courses;
   	//echo "heelllo";
    echo $courses->get_single_course();
}


genesis();
