<?php
/**
 * Template Name: Community
 *
 * A template used to demonstrate how to include the template
 * using this plugin.
 *
 * @package PTE
 * @since 	1.0.0
 * @version	1.0.0
 */

$pte = Page_Template_Plugin::get_instance();

remove_action( 'genesis_meta', 'genesis_load_stylesheet' );

add_action( 'wp_enqueue_scripts', 'ra_community_stylesheet' );
function ra_community_stylesheet() {
	wp_enqueue_style( 'community', RA_PLUGIN_URL . 'lib/css/community.css' );
}

add_action( 'genesis_meta', 'ra_community_meta' );
function ra_community_meta() {
	add_action('wp_print_styles', 'ra_community_google_webfont_enqueue');
}

// Google Web Font
function ra_community_google_webfont_enqueue() {
    wp_register_style('google-fonts', '//fonts.googleapis.com/css?family=Alfa+Slab+One|Bowlby+One|Copse');
    wp_enqueue_style( 'google-fonts');

    remove_action( 'genesis_loop', 'genesis_do_loop' );
}

remove_action( 'genesis_after_header', 'genesis_do_nav' );

// Remove Header Markup and Header
remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );
remove_action( 'genesis_header', 'genesis_header_markup_close', 15 );
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'ra_do_header' );

remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas', 4 );

remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action( 'genesis_footer', 'ra_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_cloase', 15 );

remove_action('genesis_after_footer', 'ra_do_gototop');

// Top Content
add_action( 'genesis_after_header', 'mb_do_top_area' );
function mb_do_top_area() {
	global $post;

	genesis_markup( array(
		'html5' => '<div %s>',
		'xhtml' => '<div class="wrapper">',
		'context' => 'wrapper'
	) );

	// Top Area 1
	$top_1_image = get_post_meta( $post->ID, '_ra_community_top_1_image', true );
	$top_1_image = wp_get_attachment_url( $top_1_image );
	$top_1_headline = get_the_title();
	$top_1_video = get_post_meta( $post->ID, '_ra_community_top_1_video', true );
	$top_1_button_link = get_post_meta( $post->ID, '_ra_community_top_1_button_link', true );
	$image = RA_PLUGIN_URL . 'lib/css/images/button.png';

	genesis_markup( array(
		'html5' => '<div %s><div class="wrap">',
		'xhtml' => '<div class="top-area-1"><div class="wrap">',
		'context' => 'top-area-1'
	) );

	$output .= !$top_1_image ? '' : '<div class="top-image">';
	$output .= !$top_1_image ? '' : '<img src="'.$top_1_image.'" alt="Community Logo"/>';
	$output .= !$top_1_image ? '' : '</div>';

	echo $output;

	echo '<div class="top-content">';
		echo '<h1 class="title">'.$top_1_headline.'</h1>';

		echo '<div class="video">';
			echo !$top_1_video ? '' : $top_1_video;
		echo '</div>';
		echo '<div class="button-wrapper">';
			echo '<a class="button-link" href="'.$top_1_button_link.'" title="Let Me In"><img src="'.$image.'" alt="Let Me In"/></a>';
			echo '</div>';
	echo '</div>';

	echo '</div></div>'; // End Top Area 1

	genesis_markup( array(
		'html5' => '<div %s><div class="wrap">',
		'xhtml' => '<div class="top-area-2"><div class="wrap">',
		'context' => 'top-area-2'
	) );

	echo '</div></div>'; // End Top Area 2

	echo '</div>'; // End Wrapper
}

// Middle Content


genesis();