<?php
/**
 * Template Name:  Reset-login Page Template
 *
 * Page Used: courses
 * URL : http://courses.dev/reset-login/
 */
get_header();
?>
<link rel="stylesheet" type="text/css" href="../wp-content/plugins/starter-for-genesis/lib/css/style.css" >
  <?php

    function reset_scripts() {
    $login_script_nonce = wp_create_nonce( 'reset_script_nonce' );

     wp_enqueue_script( 'reset_script',  plugins_url( '../js/reset-page.js' , __FILE__ ) , array( 'jquery' ), '1.0.0', true);
}

add_action( 'wp_enqueue_scripts', 'reset_scripts' );


    $path = dirname(dirname(dirname(__FILE__)))."/views/reset-login.php";
     include_once $path;



     // Get Parameters
     $url_email = $_GET['email'];
     $url_activation_key = $_GET['activation_key'];

     // Get Transient Values

     $transient_key = $url_email.'_ontralogin';
     $transient_activation_key = get_transient($transient_key);


     // Reset Form
     $Reset_obj = new Reset_Login(); 

     if( $url_activation_key == $transient_activation_key){
        echo $Reset_obj->render_reset_login();
        echo $Reset_obj->render_reset_login_1();
        
     } else {
        echo "Your reset link is wrong. Please reset password again: ";

     }


     

     include_once dirname(dirname(dirname(__FILE__)))."/views/member-login.php";
      $member_obj = new Member_Login();
     echo $member_obj->render_footer_widget(); 
     echo $member_obj->render_footer_widget_2(); 
     echo $member_obj->render_footer_widget_3(); 

?>


<?php get_footer(); ?>