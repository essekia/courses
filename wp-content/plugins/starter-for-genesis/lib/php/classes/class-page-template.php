<?php

/**
 * This plugin allows you to include templates with your plugin so that they can
 * be added with any theme.
 *
 * @package Page Template Example
 * @version 1.0.0
 * @since 	0.1.0
 */
class Page_Template_Plugin {

    /**
     * Plugin version, used for cache-busting of style and script file references.
     *
     * @since   1.0.0
     *
     * @var     string
     */
    const VERSION = '1.0.0';

    /**
     * Unique identifier for the plugin.
     *
     * The variable name is used as the text domain when internationalizing strings
     * of text.
     *
     * @since    1.0.0
     *
     * @var      string
     */
    protected $plugin_slug;

	/**
	 * A reference to an instance of this class.
	 *
	 * @since 0.1.0
	 *
	 * @var   Page_Template_Plugin
	 */
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	 *
	 * @var      array
	 */
	protected $templates;


	/**
	 * Returns an instance of this class. An implementation of the singleton design pattern.
	 *
	 * @return   Page_Templae_Example    A reference to an instance of this class.
	 * @since    1.0.0
	 */
	public static function get_instance() {

		if( null == self::$instance ) {
			self::$instance = new Page_Template_Plugin();
		} // end if

		return self::$instance;

	} // end getInstance

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 *
	 * @version		1.0.0
     * @since 		1.0.0
	 */
	private function __construct() {

		$this->templates = array();

		// Add a filter to the page attributes metabox to inject our template into the page template cache.
		// add_filter('page_attributes_dropdown_pages_args', array( $this, 'register_project_templates' ) );

		// Add a filter to the save post in order to inject out template into the page cache
		add_filter('wp_insert_post_data', array( $this, 'register_project_templates' ) );
		
		if ( version_compare( floatval($GLOBALS['wp_version']), '4.7', '<' ) ) { 
			add_filter( 'page_attributes_dropdown_pages_args', array( $this, 'register_project_templates' ) );
		} else { // Add a filter to the wp 4.7 version attributes metabox
			add_filter( 'theme_page_templates', array( $this, 'add_new_template' ) );
		}

		// Add a filter to the template include in order to determine if the page has our template assigned and return it's path
		add_filter('template_include', array( $this, 'view_project_template') );

		// Add your templates to this array.
		$this->templates = array(
			'template-mastermind.php'     => __( 'Mastermind Page Template', $this->plugin_slug ),
			'template-individual.php'     => __( 'Individual Courses', $this->plugin_slug ),
			'template-press.php'     => __( 'Press Page Template', $this->plugin_slug ),
			'template-about.php'     => __( 'About Page Template', $this->plugin_slug ),
			'template-content.php'     => __( 'Content Delivery Page Template', $this->plugin_slug ),
			'template-live-event.php' => __('Live Event', $this->plugin_slug),
			'template-community.php' => __('Community', $this->plugin_slug),	
			'template-community-category.php' => __('Community Category', $this->plugin_slug),				
             'template-member-login.php' => __('login Page Template',$this->plugin_slug),
             'template-forgot-password.php' => __('forgot-password Page Template',$this->plugin_slug),
             'template-reset-login.php' => __(' Reset-login Page Template',$this->plugin_slug),
             'template-mail-action.php' => __(' mail-send-action Page Template',$this->plugin_slug),
              'reset-action.php' =>  __('Reset Action Page',$this->plugin_slug),
              'template-community-course-list.php' =>  __('Courses List Template',$this->plugin_slug),
              'single-course.php' =>  __('Single Course Template',$this->plugin_slug),
              'template-community-2016-course-overview.php' =>  __('Course Overview',$this->plugin_slug),

		);


	} // end constructor

	/**
	 * Adds our template to the pages cache in order to trick WordPress
	 * into thinking the template file exists where it doens't really exist.
	 *
	 * @param   array    $atts    The attributes for the page attributes dropdown
	 * @return  array    $atts    The attributes for the page attributes dropdown
	 * @verison	1.0.0
	 * @since	1.0.0
	 */
	public function register_project_templates( $atts ) {

		// Create the key used for the themes cache
		$cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		// Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
		$templates = wp_cache_get( $cache_key, 'themes' );
		if ( empty( $templates ) ) {
			$templates = array();
		} // end if

		// Since we've updated the cache, we need to delete the old cache
		wp_cache_delete( $cache_key , 'themes');

		// Now add our template to the list of templates by merging our templates
		// with the existing templates array from the cache.
		$templates = array_merge( $templates, $this->templates );

		// Add the modified cache to allow WordPress to pick it up for listing
		// available templates
		wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		return $atts;

	} // end register_project_templates
	
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}

	/**
	 * Checks if the template is assigned to the page
	 *
	 * @version	1.0.0
	 * @since	1.0.0
	 */
	public function view_project_template( $template ) {

		global $post;

		if ( ! isset( $this->templates[ get_post_meta( $post->ID, '_wp_page_template', true ) ] ) ) {
			return $template;
		} // end if

		$file = RA_PLUGIN_DIR . '/lib/templates/' . get_post_meta( $post->ID, '_wp_page_template', true );

		// Just to be safe, we check if the file exist first
		if( file_exists( $file ) ) {
			return $file;
		} // end if

		return $template;

	} // end view_project_template


	/**
	 * Retrieves and returns the slug of this plugin. This function should be called on an instance
	 * of the plugin outside of this class.
	 *
	 * @return  string    The plugin's slug used in the locale.
	 * @version	1.0.0
	 * @since	1.0.0
	 */
	public function get_locale() {
		return $this->plugin_slug;
	} // end get_locale

} // end class
