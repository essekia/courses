<?php
// Shortcodes
function ra_list($atts, $content=null) {
    return '<span class="list">'.$content.'</span>';
}

if (shortcode_exists('list')) {
	remove_shortcode('list');
	add_shortcode('list', 'ra_list');
} else {
	add_shortcode('list', 'ra_list');
}

// Category Menu Shortcode
function category_shortcode($atts) {
    extract(shortcode_atts(array(
		'taxonomy' => 'category',
        'class' => '',
		'hierarchical' => true,
		'title_li' => '',
		'hide_empty' => false,
		'exclude' => '',
		'depth' => 0
	), $atts));

	$args = array(
		'taxonomy' => $taxonomy,
		'hierarchical' => $hierarchical,
		'title_li' => $title_li,
		'hide_empty' => $hide_empty,
		'exclude' => $exclude,
		'depth' => $depth
	);

	$output .= '<ul class="'.$class.'">';
		$output .= wp_list_categories($args);
	$output .= '</ul>';
    return $output;
}

add_shortcode('atlcat','category_shortcode');
?>