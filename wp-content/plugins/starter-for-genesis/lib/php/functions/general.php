<?php
/*
 * General Functions
 * @package Starter for Genesis
 * @version 1.0
 *
 */

add_action('genesis_setup', 'ra_general_setup', 12);
function ra_general_setup() {
	// Custom Header
	remove_action('genesis_header', 'genesis_do_header');
	add_action('genesis_header', 'ra_do_header');

	// Custom Favicon Uploader
	add_filter( 'genesis_pre_load_favicon', 'ra_favicon_filter' );

    // Custom Meta
    add_action('genesis_meta', 'ra_custom_starter_meta');
}

// Custom Meta
function ra_custom_starter_meta() {
    global $post;
    $showtitle = get_post_meta($post->ID, '_ra_page_title', true);
    $template_file = get_post_meta($post->ID, '_wp_page_template', true);

    if ($showtitle == 'on' && 'page' == get_post_type()) {
        remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
    } else {
        return;
    }

    if ( $template_file == 'template-community.php' )
        remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
}

// Custom Header
function ra_do_header() {
    $logo = ra_get_option('ra_settings_page_general', 'logo');
    global $post;
    $template_file = get_post_meta($post->ID, '_wp_page_template', true);
    echo '<div class="title-area">';
        if (!empty($logo)) {
            echo '<h1 class="site-title" itemprop="headline">';
                echo '<a href="'.get_bloginfo('url').'" class="logo" title="'.get_bloginfo('name').'">';
                    echo '<img src="'.$logo.'" alt="'.get_bloginfo('name').'"/>';
                echo '</a>';
            echo '</h1>';
        } else {
            do_action('genesis_site_title');
            do_action('genesis_site_description');
        }
    echo '</div>';
    if ($template_file != 'template-live-event.php') {
        if (is_active_sidebar('Header Right')) {
            genesis_markup(array(
                'html5' => '<aside %s>',
                'xhtml' => '<div class="widget-area header-widget-area">',
                'context' => 'widget-area'
            ));

            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Header Right')) :

            endif;
            genesis_markup(array(
                'html5' => '</aside>',
                'xhtml' => '</div>'
            ));
        }
    } else {
        if (!has_nav_menu('live-event'))
            return;

        $class = 'menu live-event-menu menu-header';
            if (genesis_superfish_enabled())
                $class .= ' js-superfish';

        $args = array(
            'theme_location' => 'live-event',
            'container'      => '',
            'menu_class'     => $class,
            'echo'           => 0,
        );

        $nav = wp_nav_menu($args);

        if (!$nav)
            return;

        $nav_markup_open = genesis_markup( array(
            'html5'   => '<nav %s>',
            'xhtml'   => '<div id="nav" class="nav-header">',
            'context' => 'nav-header',
            'echo'    => false,
        ) );

        $nav_markup_open .= genesis_structural_wrap('menu-header', 'open', 0);

        $nav_markup_close  = genesis_structural_wrap('menu-header', 'close', 0);
        $nav_markup_close .= genesis_html5() ? '</nav>' : '</div>';

        $nav_output = $nav_markup_open . $nav . $nav_markup_close;

        echo apply_filters( 'genesis_do_header_nav', $nav_output, $nav, $args );
    }

}

// Custom Favicon
function ra_favicon_filter( $favicon_url ) {
    $favicon = ra_get_option('ra_settings_page_general', 'favicon');
    if (!empty($favicon)) {
        return $favicon;
    }
}

// Add Filter to Navigation Menus
add_filter( 'genesis_nav_items', 'ra_button', 10, 2 );
add_filter( 'wp_nav_menu_items', 'ra_button', 10, 2 );

function ra_button($menu, $args) {
    $text = ra_get_option('ra_settings_page_general', 'text');
    $url = ra_get_option('ra_settings_page_general', 'link');
    $args = (array)$args;
    if ( 'primary' !== $args['theme_location'] )
        return $menu;
    $follow = '<li class="menu-item call-to-action"><a href="'.$url.'">'.$text.'</a></li>';
    return $menu . $follow;
}
?>