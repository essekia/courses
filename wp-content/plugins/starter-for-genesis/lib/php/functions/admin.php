<?php
/*
 * @package Starter for Genesis
 * @version 1.0
 *
 */
if (class_exists('RA_Settings')) {
	// Initialize Settings
	function ra_settings_page() {
		$args = array(
			'title' => 'Starter Settings',
			'slug' => 'ra_settings_page',
			'type' => 'utility',
			'icon' => 'dashicons-analytics',
			//'position' => '88.8'
		);
		global $ra_settings_page;
		$ra_settings_page = new RA_Settings($args);
	}

	add_action('admin_menu', 'ra_settings_page', 2);

	// Create General Settings Tab
	function ra_settings_page_general_new_tab($active_tab) {
		$tab_info = array(
			'title' => __('General Settings', 'genesis'),
			'slug' => 'general'
		);

		global $ra_settings_page;

		$ra_settings_page->new_tab($active_tab, $tab_info);
	}

	add_action('ra_settings_page_new_tab_hook', 'ra_settings_page_general_new_tab');

	// Create Slider Settings Tab
	function ra_settings_page_call_new_tab($active_tab) {
		$slider_info = array(
			'title' => __('Call To Action', 'genesis'),
			'slug' => 'call'
		);

		global $ra_settings_page;

		$ra_settings_page->new_tab($active_tab, $slider_info);

	}

	//add_action('ra_settings_page_new_tab_hook', 'ra_settings_page_call_new_tab');

	// Create General Settings
	function ra_settings_general_create() {
		register_setting(
			'ra_settings_page_general',
			'ra_settings_page_general',
			'ra_settings_validate'
		);
		add_settings_section(
			'general_settings',
			__('General Settings'),
			'__return_false',
			'ra_settings_page_general'
		);

		add_settings_field(
			'logo',
			__('Upload Logo', 'genesis'),
			'ra_mediaupload',
			'ra_settings_page_general',
			'general_settings',
			array(
				'name' => 'logo',
				'value' => ra_get_option('ra_settings_page_general', 'logo'),
				'description' => __('Upload your site logo.'),
				'registration' => 'ra_settings_page_general'
			)
		);

		add_settings_field(
			'favicon',
			__('Favicon', 'genesis'),
			'ra_mediaupload',
			'ra_settings_page_general',
			'general_settings',
			array(
				'name' => 'favicon',
				'value' => ra_get_option('ra_settings_page_general', 'favicon'),
				'description' => __('Set site favicon'),
				'registration' => 'ra_settings_page_general'
			)
		);

		add_settings_field(
			'text',
			__('Call To Action Text', 'genesis'),
			'ra_textbox',
			'ra_settings_page_general',
			'general_settings',
			array(
				'name' => 'text',
				'value' => ra_get_option('ra_settings_page_general', 'text'),
				'description' => __(''),
				'registration' => 'ra_settings_page_general'
			)
		);
		add_settings_field(
			'link',
			__('Call To Action Link', 'genesis'),
			'ra_textbox',
			'ra_settings_page_general',
			'general_settings',
			array(
				'name' => 'link',
				'value' => ra_get_option('ra_settings_page_general', 'link'),
				'description' => __(''),
				'registration' => 'ra_settings_page_general'
			)
		);
	}
	add_action('admin_init', 'ra_settings_general_create');
}
?>