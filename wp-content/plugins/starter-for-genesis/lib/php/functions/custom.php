<?php
// Custom Image Function
function ra_post_image() {
	global $post;
	$image = '';
	$image_id = get_post_thumbnail_id($post->ID);
	$image = wp_get_attachment_image_src($image_id, 'full');
	$image = $image[0];
	if ($image) return $image;
	return ra_get_first_image();
}
// Get the First Image Attachment Function
function ra_get_first_image() {
	global $post, $posts;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	$first_img = "";
	if (isset($matches[1][0]))
		$first_img = $matches[1][0];
	return $first_img;
}
add_action('admin_init', 'ra_admin_init');
function ra_admin_init() {
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	$pages = new Cuztom_Post_Type('page');
	$pages->add_meta_box(
		'ra_page',
		'Page Title',
		array(
			array(
				'name' => 'title',
				'label' => 'Hide Page Title',
				'description' => '',
				'type' => 'checkbox'
			)
		)
	);
	if( !isset( $post_id ) ) return;
	$template_file = get_post_meta($post_id, '_wp_page_template', true);
	switch ($template_file) {
		case 'template-mastermind.php':
			remove_post_type_support( 'page', 'editor' );
			$pages->add_meta_box(
				'ra_mastermind',
				'Additional Options',
				array(
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'video',
						'label' => 'Video Code',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'url',
						'label' => 'Button Link',
						'description' => '',
						'type' => 'text'
					),
				)
			);
			break;
		
		case 'template-community-category.php':
			$pages->add_meta_box(
				'ra_comm_category',
				'Additional Content',
				array(
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'content',
						'label' => 'content',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'button-url',
						'label' => 'Button Link',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'button-text',
						'label' => 'Button Text',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'cta',
						'label' => 'Call To Action',
						'description' => '',
						'type' => 'text'
					),
				)
			);
			break;
		
		case 'template-about.php':
			$pages->add_meta_box(
				'ra_about',
				'Additional Content',
				array(
					array(
						'name' => 'video',
						'label' => 'Video Code',
						'description' => '',
						'type' => 'textarea'
					)
				)
			);
			break;
		case 'template-content.php':
			$pages->add_meta_box(
				'ra_content',
				'Additional Content',
				array(
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'video',
						'label' => 'Video Code',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'all',
						'label' => 'Back To Videos Link',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'previous',
						'label' => 'Previous Video Link',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'next',
						'label' => 'Next Video Link',
						'description' => '',
						'type' => 'text'
					),
					/*array(
						'name' => 'button_text',
						'label' => 'Button Text',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'url',
						'label' => 'Button Link',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'progress',
						'label' => 'Progress',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'cbback',
						'label' => 'Display Back To Videos',
						'description' => '',
						'type' => 'checkbox'
					),
					array(
						'name' => 'cbtext',
						'label' => 'Back To Videos Text',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'cburl',
						'label' => 'Back to Videos URL',
						'description' => '',
						'type' => 'text'
					),*/
				),
				'normal',
				'high'
			);
			break;
		case 'template-individual.php':
			$pages->add_meta_box(
				'ra_individual',
				'Individual Course Settings',
				array(
					array(
						'name' => 'video',
						'label' => 'Course Video',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'url',
						'label' => 'Custom URL',
						'description' => '',
						'type' => 'text'
					)
				)
			);
			break;
		case 'template-live-event.php':
			remove_post_type_support( 'page', 'editor' );
			$pages->add_meta_box(
				'ra_live_event',
				'Live Event Settings',
				array(
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'video',
						'label' => 'Video Code',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'button_text',
						'label' => 'Button Text',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'url',
						'label' => 'Button Link',
						'description' => '',
						'type' => 'text'
					),
				)
			);
			$pages->add_meta_box(
				'ra_event_speakers',
				'Speakers Settings',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					)
				)
			);

			$pages->add_meta_box(
				'ra_event_agenda',
				'Agenda Settings',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'date',
						'label' => 'Event',
						'description' => '',
						'type' => 'post_select',
						'args' => array(
							'post_type' => 'event'
						),
						'repeatable' => true
					)
				)
			);
			$pages->add_meta_box(
				'ra_event_workshop',
				'Workshop Settings',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					),
				)
			);
			$pages->add_meta_box(
				'ra_event_travel',
				'Travel-Accomodation Settings',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'image',
						'label' => 'Image',
						'description' => '',
						'type' => 'image'
					),
					array(
						'name' => 'content',
						'label' => 'Content',
						'description' => '',
						'type' => 'wysiwyg'
					)
				)
			);
			$pages->add_meta_box(
				'ra_event_testimonial',
				'Testimonial Settings',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					),
				)
			);
			$pages->add_meta_box(
				'ra_event_faq',
				'FAQ Settings',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'subheadline',
						'label' => 'Subheadline',
						'description' => '',
						'type' => 'textarea'
					),
				)
			);
			break;
		case 'template-community.php':
			remove_post_type_support( 'page', 'editor' );
			$pages->add_meta_box(
				'ra_community_top_1',
				'Top Area 1',
				array(
					array(
						'name' => 'image',
						'label' => 'Community Logo',
						'description' => '',
						'type' => 'image'
					),
					array(
						'name' => 'video',
						'label' => 'Video Code',
						'description' => '',
						'type' => 'textarea'
					),
					array(
						'name' => 'button_link',
						'label' => 'Button Link',
						'description' => '',
						'type' => 'text'
					),
				),
				'normal',
				'high'
			);
			$pages->add_meta_box(
				'ra_community_top_2',
				'Top Area 2',
				array(
					array(
						'name' => 'image',
						'label' => 'Welcome Image',
						'description' => '',
						'type' => 'image'
					),
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'content',
						'label' => 'Content',
						'description' => '',
						'type' => 'wysiwyg'
					),
				),
				'normal',
				'high'
			);
			$pages->add_meta_box(
				'ra_community_middle_1',
				'Middle Area 1',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'content',
						'label' => 'Content',
						'description' => '',
						'type' => 'wysiwyg'
					),
				),
				'normal',
				'high'
			);
			$pages->add_meta_box(
				'ra_community_middle_2',
				'Middle Area 2',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'content',
						'label' => 'Content',
						'description' => '',
						'type' => 'wysiwyg'
					),
				),
				'normal',
				'high'
			);
			$pages->add_meta_box(
				'ra_community_middle_3',
				'Middle Area 3',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'content',
						'label' => 'Content',
						'description' => '',
						'type' => 'wysiwyg'
					),
				),
				'normal',
				'high'
			);
			$pages->add_meta_box(
				'ra_community_middle_4',
				'Middle Area 4',
				array(
					array(
						'name' => 'headline',
						'label' => 'Headline',
						'description' => '',
						'type' => 'text'
					),
					array(
						'name' => 'content',
						'label' => 'Content',
						'description' => '',
						'type' => 'wysiwyg'
					),
				),
				'normal',
				'high'
			);
			break;
		default:
			# code...
			break;
	}
	$posts = new Cuztom_Post_Type('post');
	$posts->add_meta_box(
		'ra_course_cat',
		'Course Settings',
		array(
			array(
				'name' => 'course_video',
				'label' => 'Course Video',
				'description' => '',
				'type' => 'textarea'
			),
			array(
				'name' => 'course_cat',
				'label' => 'Course Category',
				'description' => '',
				'type' => 'term_select',
				'args' => array(
					'taxonomy' => 'course_category'
				)
			)
		)
	);
}

add_action('wp_footer', 'fluidvids_js');
function fluidvids_js() { ?>
	<script>
	fluidvids.init({
		selector: 'iframe',
		players: ['fast.wistia.net', 'www.youtube.com']
	});
	</script>
<?php }
?>