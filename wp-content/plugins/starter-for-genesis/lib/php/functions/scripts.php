<?php
add_action('wp_enqueue_scripts', 'ra_starter_plugin_scripts');

function ra_starter_plugin_scripts() {
	if (!is_admin()) {
		wp_register_script(
			'jquery-equalize',
			RA_PLUGIN_URL . 'lib/js/equalize.min.js',
			array('jquery')
		);

		wp_enqueue_script('jquery-equalize');

		wp_register_script(
			'jquery-infinitescroll',
			RA_PLUGIN_URL . 'lib/js/jquery.infinitescroll.min.js',
			array('jquery'),
			time(),
			false
		);

		wp_enqueue_script('jquery-infinitescroll');

		wp_register_script(
			'owl-carousel',
			RA_PLUGIN_URL . 'lib/js/owl.carousel.min.js',
			array('jquery')
		);

		wp_enqueue_script('owl-carousel');

		wp_register_script(
			'fluidvids.js',
			RA_PLUGIN_URL . 'lib/js/fluidvids.js',
			array(),
			time(),
			false
		);

		wp_enqueue_script('fluidvids.js');

		wp_register_script(
			'easytabs.js',
			RA_PLUGIN_URL . 'lib/js/jquery.easytabs.min.js',
			array('jquery'),
			time(),
			true
		);

		wp_register_script(
			'tabs.js',
			RA_PLUGIN_URL . 'lib/js/tabs.js',
			array('jquery', 'easytabs.js'),
			time(),
			true
		);

		wp_register_script(
			'onepage.js',
			RA_PLUGIN_URL . 'lib/js/jquery.nav.js',
			array('jquery'),
			time(),
			true
		);

		wp_register_script(
			'single.js',
			RA_PLUGIN_URL . 'lib/js/jquery.singlePageNav.min.js',
			array('jquery'),
			time(),
			false
		);

		wp_register_script(
			'plugin-custom',
			RA_PLUGIN_URL . 'lib/js/custom.js',
			array('jquery')
		);

		wp_enqueue_script('plugin-custom');

		wp_register_style(
			'genericons-css',
			RA_PLUGIN_URL . 'lib/css/genericons.css'
		);

		wp_register_style(
			'owl-carousel-css',
			RA_PLUGIN_URL . 'lib/css/owl.carousel.css'
		);

		wp_enqueue_style('genericons-css');
		wp_enqueue_style('owl-carousel-css');

		wp_register_style(
			'starter-css',
			RA_PLUGIN_URL . 'lib/css/style.css'
		);

		wp_enqueue_style('starter-css');
	}
}
?>
