<?php
$categories = register_cuztom_taxonomy('Category', 'post');

$categories->add_term_meta(
    array(
        array(
            'name' => 'category_image',
            'label' => 'Category Image',
            'description' => '',
            'type' => 'image'
        )
    )
);

// Events
$event = new Cuztom_Post_Type('Event', array(
	'has_archive' => false,
	'supports' => array('title'),
	'menu_icon' => 'dashicons-tickets',
	'exclude_from_search' => true
));

$event->add_meta_box(
	'sfb_event',
	'Event Settings',
	array(
		array(
			'name' => 'venue',
			'label' => 'Venue',
			'description' => '',
			'type' => 'text'
		),
	)
);

$event->add_meta_box(
	'sfb_event_sched',
	'Event Schedule',
	array(
		'bundle',
		array(
			array(
				'name' => 'time',
				'label' => 'Time',
				'description' => '',
				'type' => 'text'
			),
			array(
				'name' => 'description',
				'label' => 'Description',
				'description' => '',
				'type' => 'text'
			),
		)
	)
);

// Interviews
$interview = new Cuztom_Post_Type('Interview', array(
	'has_archive' => true,
	'supports' => array('title', 'editor', 'thumbnail'),
	'menu_icon' => 'dashicons-format-chat'
));

$interview_cat = register_cuztom_taxonomy('Interview Category', 'interview');

// Products
$products = new Cuztom_Post_Type('Product', array(
	'has_archive' => false,
	'supports' => array('title', 'editor', 'thumbnail'),
	'menu_icon' => 'dashicons-cart'
));

$products->add_meta_box(
	'atl_product',
	'Other Information',
	array(
		array(
			'name' => 'video',
			'label' => 'Video Code',
			'description' => '',
			'type' => 'textarea'
		),
		array(
			'name' => 'url',
			'label' => 'Cart URL',
			'description' => '',
			'type' => 'text'
		),
	)
);

$product_cat = register_cuztom_taxonomy('Product Category', 'product');

// Testimonials
$testimonial = new Cuztom_Post_Type('Testimonial',
	array(
		'has_archive' => 'false',
		'supports' => array('title', 'thumbnail', 'editor'),
		'menu_icon' => 'dashicons-testimonial'
	)
);

$testimonial->add_taxonomy('Testimonial Category');

$testimonial->add_meta_box(
	'atl_testimonial_box',
	'Additional Options',
	array(
		array(
			'name' => 'subheadline',
			'label' => 'Subheadline',
			'description' => '',
			'type' => 'text'
		),
		array(
			'name' => 'video',
			'label' => 'Video Code',
			'description' => 'Place your video code here.',
			'type' => 'textarea'
		)
	)
);

$tools = new Cuztom_Post_Type('Tool', array(
	'has_archive' => true,
	'supports' => array('title', 'editor', 'thumbnail'),
	'menu_icon' => 'dashicons-admin-tools'
));

$tool_cat = register_cuztom_taxonomy('Tool Category', 'tool');

$speakers = new Cuztom_Post_Type('Speaker', array(
	'has_archive' => false,
	'supports' => array('title', 'excerpt', 'thumbnail'),
	'menu_icon' => 'dashicons-megaphone'
));

$speaker_cat = register_cuztom_taxonomy('Speaker Category', 'speaker');
?>