<?php
class RA_Testimonial_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_testimonial',
			'Starter - Testimonial Widget',
			array(
				'description' => __('Easily add your testimonial on your sidebar', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_numpost" => array(
				'field_id' => 'numpost',
				'field_title' => __('Number of testimonial', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'number'
			)
		);
	}

	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$numpost = $instance['numpost'];

		echo $before_widget;
		if (!empty($title))
			echo $before_title . $title . $after_title;

		global $post;
		$args = array(
			'posts_per_page' => (!empty($numpost) ? $numpost : 4),
			'post_type' => 'testimonial'
		);

		$loop = new WP_Query($args);
		$count = 1;
		if ($loop->have_posts()) :
			echo '<ul class="testimonials">';
			while ($loop->have_posts()) : $loop->the_post();

				$content = get_the_content();

				if ($count % 2 == 0) {
					$class = 'even';
				} else {
					$class = 'odd';
				}
			
				echo '<li class="testimonial '.$class.'">';
					echo '<div class = "testimonial-thumbnail">'.get_the_post_thumbnail().'</div>';
					echo '<p class="quote">'.$content.'</p>';
					echo '<h3 class="testimonial-title">'.get_the_title().'</h3>';
				echo '</li>';
				$count++;
			endwhile;
			echo '</ul>';
			wp_reset_postdata();
		endif;

		echo $after_widget;
	}
}

add_action('widgets_init', create_function('','register_widget("RA_Testimonial_Widget");' ));