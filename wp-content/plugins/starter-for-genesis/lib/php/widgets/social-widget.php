<?php
class RA_Social_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_social',
			'Starter - Social Media Widget',
			array(
				'description' => __('Easily add your social media links on your sidebar', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_twitter" => array(
				'field_id' => 'twitter',
				'field_title' => __('Twitter', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'url'
			),
			"ra_gplus" => array(
				'field_id' => 'gplus',
				'field_title' => __('Google Plus', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'url'
			),
			"ra_pinterest" => array(
				'field_id' => 'pinterest',
				'field_title' => __('Pinterest', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'url'
			),
			"ra_facebook" => array(
				'field_id' => 'facebook',
				'field_title' => __('Facebook', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'url'
			),
			"ra_linkedin" => array(
				'field_id' => 'linkedin',
				'field_title' => __('LinkedIn', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'url'
			),
			"ra_youtube" => array(
				'field_id' => 'youtube',
				'field_title' => __('Youtube', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'url'
			),
		);
	}

	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$twitter = $instance['twitter'];
		$gplus = $instance['gplus'];
		$pinterest = $instance['pinterest'];
		$facebook = $instance['facebook'];
		$linkedin = $instance['linkedin'];
		$youtube = $instance['youtube'];
		echo $before_widget;
		if (!empty($title))
			echo $before_title . $title . $after_title;

		echo '<ul class="socialmedia">';
			echo (!empty($facebook) ? '<li class="facebook"> <!--<span class="genericon genericon-facebook-alt"></span>--><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/01/facebook-icon.jpg" /><a target="_blank" href="'.$facebook.'">facebook</a></li>' : '');
			echo (!empty($pinterest) ? '<li class="pinterest"> <!--<span class="genericon genericon-pinterest"></span>--><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/01/pinterest-icon.jpg" /><a target="_blank" href="'.$pinterest.'">pinterest</a></li>' : '');
			echo (!empty($twitter) ? '<li class="twitter"> <!--<span class="genericon genericon-twitter"></span>--> <img src = "http://community.smartmarketer.com/wp-content/uploads/2016/01/twitter-icon.jpg" /><a target="_blank" href="'.$twitter.'">twitter</a></li>' : '');
			echo (!empty($gplus) ? '<li class="gplus"> <!--<span class="genericon genericon-googleplus-alt"></span>--> <img src = "http://community.smartmarketer.com/wp-content/uploads/2016/01/googleplus-icon.jpg" /><a target="_blank" href="'.$gplus.'">google plus</a></li>' : '');
			echo (!empty($linkedin) ? '<li class="linkedin"> <!--<span class="genericon genericon-linkedin"></span>--> <img src = "http://community.smartmarketer.com/wp-content/uploads/2016/01/linkedin-icon.jpg" /><a target="_blank" href="'.$linkedin.'">linkedin</a></li>' : '');
			echo (!empty($youtube) ? '<li class="youtube"> <!--<span class="genericon genericon-youtube"></span>--><img src = "http://community.smartmarketer.com/wp-content/uploads/2016/01/youtube-icon.jpg" /><a target="_blank" href="'.$youtube.'">youtube</a></li>' : '');
		echo '</ul>';

		echo $after_widget;
	}
}

add_action('widgets_init', create_function('','register_widget("RA_Social_Widget");' ));