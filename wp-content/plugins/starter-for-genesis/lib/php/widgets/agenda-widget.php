<?php
class RA_Event_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_event',
			'Starter - Event Widget',
			array(
				'description' => __('', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_event" => array(
				'field_id' => 'event',
				'field_title' => __('Event', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'selectevent'
			),
		);
	}

	public function widget($args, $instance) {
		extract($args);
		global $post;
		$title = apply_filters('widget_title', $instance['title']);
		$event = $instance['event'];

		echo $before_widget;
		if (!empty($title))
			echo $before_title . $title . $after_title;

		echo '<div class="eventwidget">';
			$args = array(
				'post_type' => 'event',
				'p' => $event
			);
			$query = new WP_Query($args);
			while ($query->have_posts()) : $query->the_post();
				$title = get_the_title();
				$venue = get_post_meta($post->ID, '_sfb_event_venue', true);
				$bundles = get_post_meta($post->ID, '_sfb_event_sched', true);
				echo '<h4 class="headline">'.$title.'</h4>';
				echo '<p class="venue">'.$venue.'</p>';
				echo '<ul class="activity">';
				foreach ($bundles as $bundle) :
					echo '<li>';
						$time = $bundle['_time'];
						$description = $bundle['_description'];
						echo '<span class="time">';
							echo $time;
						echo '</span>';
						echo '<span class="description">';
							echo $description;
						echo '</span>';
					echo '</li>';
				endforeach;	
				echo '</ul>';
			endwhile;
			wp_reset_query();
		echo '</div>';

		echo $after_widget;
	}
}

add_action('widgets_init', create_function('','register_widget("RA_Event_Widget");')); 