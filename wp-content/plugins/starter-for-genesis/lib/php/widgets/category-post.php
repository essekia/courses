<?php
class RA_Category_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_category',
			'Starter - Category Post',
			array(
				'description' => __('Easily add recent category post on your sidebar.', 'genesis'),
			)
		);

		// Build Forms
		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_category" => array(
				'field_id' => 'category',
				'field_title' => __('Category', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'selectcategory'
			),
			"ra_posttitle" => array(
				'field_id' => 'posttitle',
				'field_title' => __('Show Post Title', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'checkbox'
			),
			"ra_imagex" => array(
				'field_id' => 'imagex',
				'field_title' => __('Image Width', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'number'
			),
			"ra_imagey" => array(
				'field_id' => 'imagey',
				'field_title' => __('Image Height', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'number'
			),
			"ra_optalign" => array(
				'field_id' => 'optalign',
				'field_title' => __('Image Alignment', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'select',
				'field_select_values' => array(
					'alignnone' => 'None',
					'aligncenter' => 'Align Center',
					'alignleft' => 'Align Left',
					'alignright' => 'Align Right'
				)
			),
			"ra_optcontent" => array(
				'field_id' => 'optcontent',
				'field_title' => __('Select Content Type', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'select',
				'field_select_values' => array(
					'none' => 'Display None',
					'excerpt' => 'Excerpt',
					'full-content' => 'Full Content'
					)
			),
			"ra_excerptlen" => array(
				'field_id' => 'excerptlen',
				'field_title' => __('Excerpt Length', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'number'
			),
			"ra_showreadmore" => array(
				'field_id' => 'showreadmore',
				'field_title' => __('Show Read More', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'checkbox'
			),
			"ra_moretext" => array(
				'field_id' => 'moretext',
				'field_title' => __('Read More Text', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			/*"ra_numpost" => array(
				'field_id' => 'numpost',
				'field_title' => __('Number of Post', 'genesis'),
				'field_description' => __(''),
				'field_type' => 'number'
			),*/
		);
	}
	// Frontend View
	public function widget($args, $instance) {
		extract($args);
		global $post;

		$title = apply_filters('widget_title', $instance['title'] );
		$category = $instance['category'];
		$image = $instance['image'];
		$imagex = $instance['imagex'];
		$imagey = $instance['imagey'];
		$optalign = $instance['optalign'];
		$optcontent = $instance['optcontent'];
		$excerptlen = $instance['excerptlen'];
		$showreadmore = $instance['showreadmore'];
		$moretext = $instance['moretext'];
		$posttitle = $instance['posttitle'];
		$moretext = (!empty($moretext) ? $moretext : 'Read More &raquo;');
		/*$numpost = $instance['numpost'];*/
		$numpost = 12;

		echo $before_widget;
		if (!empty($title)) {
			echo $before_title . $title . $after_title;
		}

		$args = array(
			'posts_per_page' => (!empty($numpost) ? $numpost : 4 ),
		);

		if (!empty($category)) {
			$args['category_name'] = $category;
		}

		$query = new WP_Query($args);

		echo '<div class="category-posts">';
			while ($query->have_posts()) : $query->the_post();
				$summary = get_the_content();
				$summary = strip_shortcodes( $summary );
				// Excerpt
				if (!empty($excerptlen)) {
					$summary = wp_trim_words( $summary, $num_words = $excerptlen, $more = '...' );
				} else {
					$summary = wp_trim_words( $summary, $num_words = '50', $more = '...' );
				}
				// Check if image is present
				$image = ra_post_image();
				$image = aq_resize($image, $imagex, $imagey, true);
				// Alignment
				switch ($optalign) {
					case 'none':
						$alignment = 'alignnone';
						break;
					case 'alignright':
						$alignment = 'alignright';
						break;
					case 'aligncenter':
						$alignment = 'aligncenter';
						break;
					case 'alignleft':
					default:
						$alignment = 'alignleft';
						break;
				}

				echo '<div class="post-list">';
				// Show Page Title
				switch ($posttitle) {
					case 'showpage':
						echo '<h4 class="post-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h5>';
						break;
					default:
						echo '';
						break;
				}
				//add_action('ra_after_post_title', 'ra_do_image', 1);
				if (!empty($image)) {
					echo '<a href="'.get_permalink().'">';
						echo '<img class="featured-image '.$alignment.'" src="'.$image.'" alt="'.get_the_title().'"/>';
					echo '</a>';
				}
				// Content Type
				switch ($optcontent) {
					case 'none':
						echo '';
						break;
					case 'full-content':
						global $more;
						$more = 0;
						the_content($moretext);
						break;
					case 'excerpt':
					default:
						echo '<p>'.$summary.'</p>';
						break;
				}

				if (($showreadmore == 'showreadmore')) {
					echo '<a href="'.get_permalink().'" class="read-more">'.$moretext.'</a>';
				}
				echo '</div>';
			endwhile;
		echo '</div>';
		echo $after_widget;
	}
}

add_action('widgets_init', create_function('', 'register_widget("RA_Category_Widget");'));
?>