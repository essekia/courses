<?php
class RA_Iconic_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_iconic',
			'Starter - Iconic Widget',
			array(
				'description' => __('', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_subheadline" => array(
				'field_id' => 'subheadline',
				'field_title' => __('Subheadline', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textarea'
			),
			"ra_image" => array(
				'field_id' => 'image',
				'field_title' => __('Upload Image', 'textdomain'),
				'field_description' => '',
				'field_type' => 'mediaupload'
			),

			"ra_imagex" => array(
				'field_id' => 'imagex',
				'field_title' => 'Image Width',
				'field_description' => '',
				'field_type' => 'number'
			),

			"ra_imagey" => array(
				'field_id' => 'imagey',
				'field_title' => 'Image Height (*Optional)',
				'field_description' => '',
				'field_type' => 'number'
			),

			"ra_imagecrop" => array(
				'field_id' => 'imagecrop',
				'field_title' => 'Crop Image',
				'field_description' => '',
				'field_type' => 'checkbox'
			),

			"ra_alignment" => array(
				'field_id' => 'alignment',
				'field_title' => 'Alignment',
				'field_description' => '',
				'field_type' => 'select',
				'field_select_values' => array(
					'alignnone' => 'Align None', 
					'alignleft' => 'Align Left',
					'aligncenter' => 'Align Center', 
					'alignright' => 'Align Right'
				)
			),
		);
	}

	public function widget($args, $instance) {
		extract($args);
		// $title = apply_filters('widget_title', $instance['title']);
		$title = $instance['title'];
		$subheadline = $instance['subheadline'];
		$image = $instance['image'];
		$imagex = $instance['imagex'];
		$imagey = $instance['imagey'];
		$imagecrop = $instance['imagecrop'];
		$alignment = $instance['alignment'];

		echo $before_widget;
		
		echo '<div class="iconicwidget">';
			
			if (!empty($imagex)) {
				$image = aq_resize($image, $imagex, (!empty($imagecrop) ? true : false));
			} elseif (!empty($imagex) && !empty($imagey)) {
				$image = aq_resize($image, $imagex, $imagey, $imagecrop);
			} else {
				$image = $image;
			}
			
			echo (!empty($image) ? '<img src="'.$image.'" class="'.(!empty($alignment) ? $alignment : 'alignnone').'"/>' : '');

			if (!empty($title))
				echo $before_title . $title . $after_title;
			echo (!empty($subheadline) ? wpautop($subheadline, false) : '');

		echo '</div>';

		echo $after_widget;
	}
}

add_action('widgets_init', create_function('','register_widget("RA_Iconic_Widget");' )); 