<?php
class RA_Tabbed_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_tabbed',
			'Starter - Tabbed Widget',
			array(
				'description' => __('Turn your sidebars into tabbed widgets.', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'sidebar',
				'field_title' => __('Select Sidebar', 'textdomain'),
				'field_description' => __('Please do not add the sidebar in which this sidebar will reside.'),
				'field_type' => 'selectsidebar'
			)
		);
	}
	public function widget($args, $instance) {
		add_filter('dynamic_sidebar_params', array(&$this, 'widget_sidebar_params'));
		
		extract($args, EXTR_SKIP);

		wp_enqueue_script('easytabs.js');
		wp_enqueue_script('tabs.js');
		
		echo $before_widget;
			if ($args['id'] != $instance['sidebar']) {
                echo '<div id="tab-container" class="tab-container">';
                    echo '<ul class="etabs"></ul>';
                    dynamic_sidebar($instance['sidebar']);
                echo '</div>';
            } else {
                echo 'Tabber widget is not properly configured.';
            }
		echo $after_widget;
		
		remove_filter('dynamic_sidebar_params', array(&$this, 'widget_sidebar_params'));
	}

	public function widget_sidebar_params($params) {
		$id = $params[0]['widget_id'];
        $params[0]['before_widget'] = '<div id="'.$params[0]['widget_id'].'" class="tabs">';
        $params[0]['after_widget'] = '</div>';
        $params[0]['before_title'] = '<a href="#" class="tab-title" id="tab-'.$id.'">';
        $params[0]['after_title'] = '</a>';
        return $params;
    }
}

add_action('widgets_init', create_function('','register_widget("RA_Tabbed_Widget");' ));