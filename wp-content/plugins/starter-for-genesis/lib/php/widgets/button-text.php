<?php
class RA_Text_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_text',
			'Starter - Custom Text',
			array(
				'description' => __('', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_textarea" => array(
				'field_id' => 'textarea',
				'field_title' => __('Text', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textarea'
			),
			"ra_btntext" => array(
				'field_id' => 'btntext',
				'field_title' => __('Button Text', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
			"ra_btnlink" => array(
				'field_id' => 'btnlink',
				'field_title' => __('Button Link', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),
		);
	}

	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$textarea = $instance['textarea'];
		$btntext = $instance['btntext'];
		$btnlink = $instance['btnlink'];

		echo $before_widget;
		if (!empty($title))
			echo $before_title . $title . $after_title;

		echo '<div class="customtext">';
			echo (!empty($textarea) ? wpautop($textarea, true) : '');
			echo (!empty($btnlink) ? '<a class="register" href="'.$btnlink.'">' : '');
				echo (!empty($btntext) ? $btntext : '');
			echo (!empty($btnlink) ? '</a>' : '');
		echo '</div>';

		echo $after_widget;
	}
}

add_action('widgets_init', create_function('','register_widget("RA_Text_Widget");' ));