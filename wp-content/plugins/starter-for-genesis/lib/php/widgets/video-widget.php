<?php
class RA_Video_Widget extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_video',
			'Starter - Video Widget',
			array(
				'description' => __('Easily add videos to your sidebar.', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),

			"ra_video" => array(
				'field_id' => 'video',
				'field_title' => 'Video Code',
				'field_description' => '',
				'field_type' => 'textarea'
			)

		);
	}

	public function widget($args, $instance) {
		extract($args);
		$title = apply_filters('widget_title', $instance['title']);
		$video = $instance['video'];

		echo $before_widget;
		if (!empty($title))
			echo $before_title . $title . $after_title;

		echo '<div class="videowidget">';
			echo (!empty($video) ? $video : '');
		echo '</div>';

if (!empty($title))
			echo $before_title . $title . $after_title;

		echo $after_widget;
	}
}

add_action('widgets_init', 'ra_register_video_widget');
function ra_register_video_widget() {
	register_widget('RA_Video_Widget');
}
?>