<?php
class RA_Image_Widget2 extends RA_Widget {
	public function __construct() {
		parent::__construct(
			'ra_image2',
			'Smart Marketer - Image Widget',
			array(
				'description' => __('Easily add images to your sidebar.', 'textdomain')
			)
		);

		$this->_form = array(
			"ra_title" => array(
				'field_id' => 'title',
				'field_title' => __('Title', 'textdomain'),
				'field_description' => __(''),
				'field_type' => 'textbox'
			),


			"ra_image" => array(
				'field_id' => 'image',
				'field_title' => __('Upload Image', 'textdomain'),
				'field_description' => '',
				'field_type' => 'mediaupload'
			),

			"ra_imagex" => array(
				'field_id' => 'imagex',
				'field_title' => 'Image Width',
				'field_description' => '',
				'field_type' => 'number'
			),

			"ra_imagey" => array(
				'field_id' => 'imagey',
				'field_title' => 'Image Height (*Optional)',
				'field_description' => '',
				'field_type' => 'number'
			),

			"ra_imagecrop" => array(
				'field_id' => 'imagecrop',
				'field_title' => 'Crop Image',
				'field_description' => '',
				'field_type' => 'checkbox'
			),

			"ra_alignment" => array(
				'field_id' => 'alignment',
				'field_title' => 'Alignment',
				'field_description' => '',
				'field_type' => 'select',
				'field_select_values' => array('alignnone' => 'Align None', 'alignleft' => 'Align Left', 'alignright' => 'Align Right')
			),

			"ra_link" => array(
				'field_id' => 'imagelink',
				'field_title' => 'Image Link',
				'field_description' => '',
				'field_type' => 'url'
			)
		);
	}

	public function widget($args, $instance) {
		extract($args);
		$title =  $instance['title'];
		$image = $instance['image'];
		$imagex = $instance['imagex'];
		$imagey = $instance['imagey'];
		$imagecrop = $instance['imagecrop'];
		$imagelink = $instance['imagelink'];
		$alignment = $instance['alignment'];

		// print_r( $imagecrop );
		if ( $imagelink )
			$open = '<a href="'.$imagelink.'">';
			$close = '</a>';

		echo $before_widget;
	
		if ( !$imagecrop ) :
			$imagecrop = false;
		else :
			$imagecrop = true;
		endif;

		echo '<div class="imagewidget">';

			if (!empty($imagex)) {
				// $image = aq_resize($image, $imagex, true );
				// $image = aq_resize( $image, 220, 220, true );
				$image = aq_resize( $image, $imagex, $imagey, $imagecrop );
			// } elseif (!empty($imagex) && !empty($imagey)) {
				// $image = aq_resize($image, $imagex, $imagey, $imagecrop);
			} else {
				$image = $image;
			}

				echo (!empty($imagelink) ? '<a href="'.$imagelink.'">' : '');
				echo (!empty($image) ? '<img src="'.$image.'" class="'.(!empty($alignment) ? $alignment : 'alignnone').'"/>' : '');
				echo (!empty($imagelink) ? '</a>' : '');

				if (!empty($title))
			echo "<h4>". $title ."</h4>";
		
		echo '</div>';

		echo $after_widget;
	}
}

add_action('widgets_init', 'ra_register_image_widget2');
function ra_register_image_widget2() {
	register_widget('RA_Image_Widget2');
}
?>