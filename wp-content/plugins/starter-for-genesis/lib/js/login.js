// (function($){
	jQuery(document).ready(function(){

		console.log('login.js');


		jQuery(".member-login-section #wp-submit").click(function(e){
			e.preventDefault();


			var loading_icon = "<img src='"+pluginsDirURL+"/starter-for-genesis/lib/images/Ellipsis.gif'/>";
			var notification = "<span class='login-notification'>"+loading_icon+"</span>";
			
			 if( jQuery('.member-login-section .login-notification').length <= 0){
                	jQuery(notification).insertAfter('.member-login-section .login-remember');
                } else { // if exist
                	jQuery('.member-login-section .login-notification').html(loading_icon);
                }

			var email = jQuery('.member-login-section #user_login').val();
			var password = jQuery('.member-login-section #user_pass').val();
			console.log('email: ' + email);
			console.log('password: ' + password);

			var nonce = loginScriptNonce;
			console.log('nonce: ' + nonce);
			console.log('my_ajax_object.ajax_url: ' + my_ajax_object.ajax_url);

			var data = {
                'action': 'ls_courses_login',
                'nonce': nonce,
                'email': email,
                'password': password // We pass php values differently!
            };

            jQuery.post(my_ajax_object.ajax_url, data, function(response) {

                // var ajaxResponse = JSON.parse(response);

                // If login-notification does not exist

                var className = 'fail';
                if( response == 'Correct Password'){
                	var className = 'success'; 
                }
                var notification = "<span class='login-notification "+className+"'>"+response+"</span>";

                if( jQuery('.member-login-section .login-notification').length <= 0){
                	jQuery(notification).insertAfter('.member-login-section .login-remember');
                } else { // if exist
                	jQuery('.member-login-section .login-notification').addClass(className);
                	jQuery('.member-login-section .login-notification').html(response);
                }
                
                if( response == 'Correct Password'){
                	window.location = siteURL + "/courses";
                }
                console.log("from server: : " + response);
            });

      

		});



	});
// });