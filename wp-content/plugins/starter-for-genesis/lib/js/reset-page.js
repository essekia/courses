// (function($){
	jQuery(document).ready(function(){

		console.log('reset-page.js');

		jQuery( "#reset_password_send-2-pass-input, #reset_password_send-1-pass-input" ).keyup(function() {
			var confirm_password = jQuery( "#reset_password_send-2-pass-input" ).val();
			var password = jQuery('#reset_password_send-1-pass-input').val();

            if( password == confirm_password){
                if( jQuery('#reset-notice').length <= 0 ){
                   // jQuery("<p id='reset-notice'>Passwords do not match.</p>").insertAfter("#reset_password_send-2-pass-input"); 
               } else{
                    jQuery("#reset-notice").html(""); 
               }
            } else{
                if( jQuery('#reset-notice').length <= 0 ){
                   jQuery("<p id='reset-notice'>Passwords do not match.</p>").insertAfter("#reset_password_send-2-pass-input"); 
               } else{
                    jQuery("#reset-notice").html("Passwords do not match."); 
               }
                
            }
		});


        jQuery("#reset_password_send-1").submit(function(e){
            // e.preventDefault();
            

            var password = jQuery('#reset_password_send-1-pass-input').val();
            var confirm_password = jQuery( "#reset_password_send-2-pass-input" ).val();
            console.log('password: ' + password );
            console.log('confirm_password: ' + confirm_password );

            
            if( password == confirm_password){
                // e.preventDefault();
                return true;
            } else {
                //Prevent the submit event and remain on the screen
                e.preventDefault();
                return false;
            }


            
        });

	});
// });