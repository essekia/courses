jQuery(function($){
	$('.tab-container').each(function() {
        var ul = $(this).find('ul.etabs');

        $(this).children('div.tabs').each(function(e) {
            var widget = $(this).attr('id');
            //var wi = $(window).width();

            $(this).find('a.tab-title').attr('href', '#' + widget).wrap('<li class="tab"></li>').parent().detach().appendTo(ul);
        });

        var wi = $(window).width();
        $(this).easytabs({
            animate: true,
            updateHash: false,
        });

    });
});
