(function($) {

    $(document).ready(function() {
        $('.category-posts').owlCarousel({
            items: 4,
            itemsDesktop: [1000, 4],
            itemsTablet: [747, 2],
            itemsMobile: false,
            navigation: true
        });

        $('.owl-next').empty();
        $('.owl-prev').empty();

        var $window = $(window);

        function checkwidth() {
            var windowsize = $window.width();
            if (windowsize <= 1023) {
                var stickyNavTop = $('.site-header').offset().top;

                var stickyNav = function() {
                    var scrollTop = $(window).scrollTop();

                    if (scrollTop > stickyNavTop) {
                        $('.nav-primary').addClass('sticky');
                        //$('.wrap').css('margin-top', 100);
                    } else {
                        $('.nav-primary').removeClass('sticky');
                        //$('.wrap').removeClass('addmargin');
                    }
                };

                stickyNav();

                $(window).scroll(function() {
                    stickyNav();
                });
            }
        }

        checkwidth();

        $(window).resize(checkwidth);

        $('nav .wrap').prepend('<h4 class="menu-btn">Navigation <span class="dashicons dashicons-menu"></span></h4>');
        $('.nav-header a').prepend('<span class="icon"></span>');

        $('.menu-btn').click(function() {
            var menubtn = $(this).next('ul');

            menubtn.slideToggle('slow');
            $(this).toggleClass('close');
            event.preventDefault();
        });

        var headingheight = $('.widget_ra_iconic .widgettitle').height();



        $(".module-title").each(function(index) {
            $(this).click(function() {
                console.log("module clicked....");
                if ($(this).hasClass("module-active-link")) {
                    for (x = 1; x <= 99; x++) {
                        var contentClass = ".module-content-" + x;
                        var moduleClass = "module-title-" + x;
                        if ($(this).hasClass(moduleClass)) {
                            console.log("moduleClass: " + moduleClass);
                            console.log("moduleClass: " + contentClass);
                            for (x = 1; x <= 99; x++) {
                                var content = ".module-content-" + x;
                                var noAccess = ".module-no-access-" + x;
                                $(content).css("display", "none");
                                $(noAccess).css("display", "none");
                            } //endfor
                            $(contentClass).css("display", "block");
                        } //endif
                    } //endfor
                } //endif
                else {
                    for (x = 1; x <= 99; x++) {
                        var contentClass = ".module-no-access-" + x;
                        var moduleClass = "module-title-" + x;
                        if ($(this).hasClass(moduleClass)) {
                            for (x = 1; x <= 99; x++) {
                                var content = ".module-content-" + x;
                                var noAccess = ".module-no-access-" + x;
                                $(content).css("display", "none");
                                $(noAccess).css("display", "none");
                            } //endfor
                            $(contentClass).css("display", "block");
                        } //endif

                    } //endfor
                }
            });
        });




        $(".course-content-right a").click(function() {
            $(".single-sfwd-topic input").css("background-color", "#55b867");


        })


        $(".content-headline").each(function(index) {
            $(this).click(function() {
                $(this).toggleClass("close-button");
                $(this).parent().children("div.content-area").slideToggle("fast");
            });
        });

    });


    $(window).load(function() {
        $(".footer-widgets .wrap").equalize();
        $('.category-lists').equalize({
            children: 'section'
        });
        $('.speaker-sidebar, .workshop-widget').equalize({
            children: 'div.widget-wrap'
        });
    });
})(jQuery);
