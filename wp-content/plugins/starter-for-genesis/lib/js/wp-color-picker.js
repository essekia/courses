jQuery(document).ready(function($){
    function updateColorPickers(){
        $('.of-color').each(function(){
            $(this).wpColorPicker({
                // you can declare a default color here,
                // or in the data-default-color attribute on the input
                defaultColor: false,
                // a callback to fire whenever the color changes to a valid color
                change: function(event, ui){},
                // a callback to fire when the input is emptied or an invalid color
                clear: function() {},
                // hide the color picker controls on load
                hide: true,
            });
        });
    }

    $(document).ajaxSuccess(function(e, xhr, settings) {

        if(settings.data.search('action=save-widget') != -1 ) {
            updateColorPickers();
        }
    });
});