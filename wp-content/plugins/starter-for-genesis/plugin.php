<?php
/**
 * Plugin Name: Starter for Genesis
 * Plugin URI: http://www.superfastbusiness.com
 * Description: A plugin that extends the Starter for Genesis theme functionality that includes widget class extender, aq_resizer, admin page class and many more.
 * Version: 1.1
 * Author: SuperFastBusiness
 * Author URI: http://www.superfastbusiness.com
 * License: GPL2
 */

define('STARTER_GENESIS_PATH', plugin_dir_path(__FILE__));

// Prevent Direct Access To Plugin
if ( !defined( 'ABSPATH' ) ) {
    wp_die( __( "Sorry, you are not allowed to access this page directly.", 'textdomain' ) );
}

// Check if Genesis is installed
register_activation_hook(__FILE__, 'ra_up_activation_check');
function ra_up_activation_check() {
	$theme_info = get_theme_data(TEMPLATEPATH.'/style.css');

	// need to find a way to check active themes is MultiSites	- This does not work in new 3.1 network panel.
	if( basename(TEMPLATEPATH) != 'genesis' ) {
		deactivate_plugins(plugin_basename(__FILE__)); // Deactivate ourself
		wp_die('Sorry, you can\'t activate unless you have installed <a href="http://www.studiopress.com/themes/genesis">Genesis</a>');
	}
}


 

// function ls_courses_login(){
// 	echo "ls_courses_login";
// 	wp_die();
// }

// add_action('wp_ajax_ls_courses_login', 'ls_courses_login');
// add_action('wp_ajax_nopriv_ls_courses_login', 'ls_courses_login');

function courses_before_header_hooks(){
        // AJAX ACTIONs
        error_log('LS_COURSES_HEADER before_header_hooks');
        $customer_model_dir = plugin_dir_path(__FILE__).'/models/customer-model.php';
        include_once $customer_model_dir;
        $customer_model = new LS_COURSES_CUSTOMER_MODEL();

        // add_action('wp_ajax_ls_courses_login', 'ls_courses_login');
        // add_action('wp_ajax_nopriv_ls_courses_login', 'ls_courses_login');
        // add 'ajax' action when not logged in

        // END AJAX ACTIONs
}


add_action('plugins_loaded', 'courses_before_header_hooks');







// Define Constants
if ( !defined( 'RA_PLUGIN_URL' ) )
	define( 'RA_PLUGIN_URL', plugins_url( '', __FILE__ ) . '/' );
if ( !defined( 'RA_PLUGIN_DIR' ) )
	define( 'RA_PLUGIN_DIR', dirname( __FILE__ ) );

// Add Widget Class
include_once RA_PLUGIN_DIR . '/lib/php/classes/widget-class.php';

// Add AQ Resizer
include_once RA_PLUGIN_DIR . '/lib/php/classes/aq_resizer.php';

// Add Settings Class
include_once RA_PLUGIN_DIR . '/lib/php/classes/settings-class.php';

// Add Cuztom Helper Class
include_once RA_PLUGIN_DIR . '/lib/php/classes/cuztom/cuztom.php';

// Page Template on Plugin
require_once RA_PLUGIN_DIR . '/lib/php/classes/class-page-template.php';
add_action( 'plugins_loaded', array( 'Page_Template_Plugin', 'get_instance' ) );

// Header - Custom
require_once RA_PLUGIN_DIR . '/views/header.php';
$header = new LS_COURSES_HEADER();



// Add Widgets
$widgets = glob( RA_PLUGIN_DIR . '/lib/php/widgets/*.php', GLOB_NOSORT);
if ( is_array( $widgets) ) {
	foreach ( $widgets as $widget ) {
		include_once $widget;
	}
}

// Functions
$functions = glob( RA_PLUGIN_DIR . '/lib/php/functions/*.php', GLOB_NOSORT );
$function_count = 0;
if ( is_array( $functions ) ) {
    foreach ($functions as $function) {
        // error_log("function_count: " . $function_count , 0);
        $function_count++;
        include_once $function;
    }
}

// Initialize Plugin
class RA_PLUGIN {

	/**
	 * Initialize the plugin and call the appropriate hook methods
	 *
	 * @uses ra_admin_hooks
	 * @author ATLWeb
	 */
	function __construct() {
		if ( is_admin() )
			add_action( 'init', array( $this, 'ra_admin_hooks' ) );
	}

	/**
	* Setup appropriate hooks for wp-admin
	*
	* @return void
	* @author ATLWeb
	*/
	function ra_admin_hooks() {
		add_action( 'admin_enqueue_scripts', array( $this, 'ra_widget_enqueue_scripts' ) );
	}


    function ra_frontend_enqueue_scripts(){
        error_log ("ra_frontend_enqueue_scripts: " . RA_PLUGIN_URL, 0);
        wp_enqueue_script('ra-custom-js', RA_PLUGIN_URL . 'lib/js/custom.js', array('jquery'));
    }
	/**
	* Add scripts
	*
	* @return void
	* @author ATLWeb
	*/
	function ra_widget_enqueue_scripts(){
		//Get current page
		$current_page = get_current_screen();

		//Only load if we are not on the nav menu page - where some of our scripts seem to be conflicting
		if ( $current_page->base != 'nav-menus' ){

			// Settings Class CSS
			wp_enqueue_style('settings-class-css', RA_PLUGIN_URL . 'lib/css/settings-class.css');

			// Image Upload
			wp_enqueue_media();
			wp_enqueue_script( 'widget-image-upload', RA_PLUGIN_URL . 'lib/js/image-upload.js', array( 'jquery' ) );

			// Color Picker
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'widget-wp-color-picker-load', RA_PLUGIN_URL . 'lib/js/wp-color-picker.js', array( 'wp-color-picker' ), false, true );

			// Dashicons
		    wp_enqueue_style( 'dashicons-picker', RA_PLUGIN_URL . 'lib/css/dashicons-picker.css' , array( 'dashicons' ), '1.0' );
		    wp_enqueue_script( 'dashicons-picker', RA_PLUGIN_URL . 'lib/js/dashicons-picker.js', array( 'jquery' ), '1.0' );

		    // Date Picker
		    wp_enqueue_script( 'field-date-js', RA_PLUGIN_URL . 'lib/js/field-date.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker'), time(), true );
		    wp_enqueue_style( 'jquery-ui-datepicker', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );
		}
	}
}

$ra_plugin = new RA_PLUGIN();
?>
