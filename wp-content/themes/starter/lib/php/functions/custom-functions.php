<?php
// Additional Widget Class
add_filter( 'dynamic_sidebar_params', 'ra_widget_classes' );
function ra_widget_classes( $params ) {
    global $ra_widget_num; // Global a counter array
    $this_id = $params[0]['id']; // Get the id for the current sidebar we're processing
    $arr_registered_widgets = wp_get_sidebars_widgets(); // Get an array of ALL registered widgets
    if ( !$ra_widget_num ) {// If the counter array doesn't exist, create it
        $ra_widget_num = array();
    }
    if ( !isset( $arr_registered_widgets[$this_id] ) || !is_array( $arr_registered_widgets[$this_id] ) ) { // Check if the current sidebar has no widgets
        return $params; // No widgets in this sidebar... bail early.
    }
    if ( isset($ra_widget_num[$this_id] ) ) { // See if the counter array has an entry for this sidebar
        $ra_widget_num[$this_id] ++;
    } else { // If not, create it starting with 1
        $ra_widget_num[$this_id] = 1;
    }
    $class = 'class="widget widget-' . $ra_widget_num[$this_id] . ' '; // Add a widget number class for additional styling options
    if ( $ra_widget_num[$this_id] == 1 ) { // If this is the first widget
        $class .= 'widget widget-first ';
    } elseif( $ra_widget_num[$this_id] == count( $arr_registered_widgets[$this_id] ) ) { // If this is the last widget
        $class .= 'widget widget-last ';
    }
    $counted = count_sidebar_widgets($this_id, false);
    if ($counted == 1) {
        $class .= 'widget-full ';
    } elseif ($counted == 2) {
        $class .= 'widget-half ';
    } elseif ($counted == 3) {
        $class .= 'widget-third ';
    } elseif ($counted == 4) {
        $class .= 'widget-fourth ';
    } elseif ($counted == 5) {
        $class .= 'widget-fifth ';
    } elseif ($counted == 6) {
        $class .= 'widget-sixth ';
    } else {
        $class .= '';
    }
    $params[0]['before_widget'] = str_replace( 'class="widget ', $class, $params[0]['before_widget'] ); // Insert our new classes into "before widget"
    return $params;
}

// Count Sidebar Widgets
function count_sidebar_widgets( $sidebar_id, $echo = true ) {
    $the_sidebars = wp_get_sidebars_widgets();
    if( !isset( $the_sidebars[$sidebar_id] ) )
        return __( 'Invalid sidebar ID' );
    if( $echo )
        echo count( $the_sidebars[$sidebar_id] );
    else
        return count( $the_sidebars[$sidebar_id] );
}

// Automatic Copyright
function ra_copyright() {
    global $wpdb;
    $copyright_dates = $wpdb->get_results("
    SELECT
    YEAR(min(post_date_gmt)) AS firstdate,
    YEAR(max(post_date_gmt)) AS lastdate
    FROM
    $wpdb->posts
    WHERE
    post_status = 'publish'
    ");
    $output = '';
    if($copyright_dates) {
        $copyright = $copyright_dates[0]->firstdate;
        if($copyright_dates[0]->firstdate != $copyright_dates[0]->lastdate) {
            $copyright .= '-' . $copyright_dates[0]->lastdate;
        }
        $output = $copyright;
    }
    return $output;
}

function get_ra_url($path = __FILE__, $url = array()){
    $path = dirname( $path );
    $path = str_replace( '\\', '/', $path );
    $explode_path = explode( '/', $path );

    $current_dir = $explode_path[count( $explode_path ) - 1];
    array_push( $url, $current_dir );

    if( $current_dir == 'wp-content' )
    {
        // Build new paths
        $path = '';
        $directories = array_reverse( $url );

        foreach( $directories as $dir )
        {
            $path = $path . '/' . $dir;
        }

        return $path;
    }
    else
    {
        return $this->get_ra_url( $path, $url );
    }
}

if ( ! function_exists( 'unregister_post_type' ) ) :
function unregister_post_type( $post_type = 'course') {
    global $wp_post_types;
    if ( isset( $wp_post_types[ $post_type ] ) ) {
        unset( $wp_post_types[ $post_type ] );
        return true;
    }
    return false;
}
endif;

//unregister_post_type('course');

//add_action('genesis_meta', 'ra_search_view');
function ra_search_view() {
    if (is_search()) {
        remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
        remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
        remove_action('genesis_entry_content', 'genesis_do_post_content');
        add_action( 'genesis_before_entry', 'ra_do_post_image');
    }
}


function ra_do_post_image() {
    $image = ra_post_image();
    $image = aq_resize($image, 300);
    if (!empty($image)) {
        echo '<a href="'.get_permalink().'" class="alignleft">';
        echo '<img src="'.$image.'" alt="'.get_the_title().'" class="alignleft"/>';
        echo '</a>';
    }
}