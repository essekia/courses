<?php
// jQuery Scripts
function ra_general_scripts() {
	if (!is_admin()) {
		wp_enqueue_script(
			'retina.js',
			get_stylesheet_directory_uri() . '/lib/js/retina-1.1.0.min.js',
			array(),
			time(),
			true
		);
		wp_enqueue_script(
			'infinitescroll.js',
			get_stylesheet_directory_uri() . '/lib/js/jquery.infinitescroll.min.js',
			array( 'jquery' ),
			'1.0',
			true
		);
		wp_enqueue_script(
			'gridalicious.js',
			get_stylesheet_directory_uri() . '/lib/js/jquery.grid-a-licious.min.js',
			array( 'jquery' ),
			'1.0',
			true
		);
		wp_register_script(
			'ra-custom',
			get_stylesheet_directory_uri() . '/lib/js/custom.js',
			array('jquery'),
			time(),
			true
		);
		wp_enqueue_script('ra-custom');

		if (is_page_template('template-live-event.php')) {
			wp_register_style(
				'live-event',
				get_stylesheet_directory_uri() . '/lib/css/live-event.css'
			);

			wp_enqueue_style('live-event');
		}
	}
}

add_action('wp_enqueue_scripts', 'ra_general_scripts');