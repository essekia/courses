<?php
/**
 * Functions
 *
 * @package      Starter for Genesis
 * @since        0.1
 * @link         http://www.superfastbusiness.com
 * @author       SuperFastBusiness <www.superfastbusiness.com>
 * @copyright    Copyright (c) 2014, SuperFastBusiness
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

/**
 * Theme Setup
 * @since 1.0.0
 *
 * This setup function attaches all of the site-wide functions
 * to the correct hooks and filters. All the functions themselves
 * are defined below this setup function.
 *
 */

add_action ('genesis_header', 'load_jquery_library');
function load_jquery_library(){
 wp_enqueue_script('jquery');
}

// add_action ('genesis_footer', 'load_intercom_scripts');
// function load_intercom_scripts(){
// 	wp_enqueue_script();
// }

add_action('genesis_setup', 'ra_child_theme_setup', 15);
function ra_child_theme_setup() {

    global $detect;
    global $mobile;
    global $tablet;

    // Define CSS Theme Version
    define( 'CHILD_THEME_VERSION', filemtime( get_stylesheet_directory() . '/style.css' ) );

    // Child theme (do not remove)
    define( 'RA_THEME_NAME', 'Starter for Genesis' );
    define( 'RA_THEME_URL', 'http://www.superfastbusiness.com/' );
    define( 'RA_THEME_INC', CHILD_DIR . '/lib/' );
    define( 'RA_THEME_INC_URL', CHILD_URL . '/lib/' );
    define( 'RA_THEME_FUNC', CHILD_DIR . '/lib/php/functions/' );
    define( 'RA_THEME_CLASS', CHILD_DIR . '/lib/php/classes/' );

    // Mobile Detection
    if (!class_exists('Mobile_Detect')) {
        require_once CHILD_DIR . '/lib/php/classes/Mobile_Detect.php';
    }

    $detect = new Mobile_Detect;
    $mobile = $detect->isMobile();
    $tablet = $detect->isTablet();

    // Include Required PHP Files
    $includephps = glob(RA_THEME_INC . 'php/functions/*.php', GLOB_NOSORT);
    if (is_array($includephps)) {
    	foreach ($includephps as $includephp) {
    		require_once $includephp;
    	}
    }

    // Remove Genesis Admin Menu
    //remove_theme_support( 'genesis-admin-menu' );

    // Genesis 2.0 HTML5 Structure
    add_theme_support( 'html5' );

    // Add support for 3-column footer widgets
    add_theme_support( 'genesis-footer-widgets', 3 );

    // Structural Wraps
    add_theme_support( 'genesis-structural-wraps', array(
        'header',
        'nav',
        'site-inner',
        'footer-widgets',
        'footer',
        'home-featured',
        'home-top',
        'home-bottom',
        'optin-box',
        'speakers',
        'agenda',
        'workshops',
        'tickets',
        'travel-accommodation',
        'testimonials',
        'faq'
    ) );

    // Add Color Options
    add_theme_support('genesis-style-selector', array(
        'starter-black' => __('Black', 'genesis'),
        'starter-blue' => __('Blue', 'genesis')
    ));

    // Navigation Menus
    remove_theme_support ( 'genesis-menus' );
    add_theme_support ( 'genesis-menus' , array (
        'primary' => 'Primary Navigation Menu' ,
        'live-event' => 'Live Event Navigation Menu',
    ) );

    // Add viewport meta tag for mobile browsers
    add_theme_support( 'genesis-responsive-viewport' );

    // Unregister site layouts
    genesis_unregister_layout( 'sidebar-content' );
    genesis_unregister_layout( 'content-sidebar-sidebar' );
    genesis_unregister_layout( 'sidebar-sidebar-content' );
    genesis_unregister_layout( 'sidebar-content-sidebar' );

    // Unregister unneeded sidebars
    unregister_sidebar( 'sidebar-alt' );
    // unregister_sidebar( 'sidebar' );

    // Cleanup WP Head
    remove_action( 'wp_head', 'rsd_link' );
    remove_action( 'wp_head', 'wlwmanifest_link' );
    remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'start_post_rel_link' );
    remove_action( 'wp_head', 'index_rel_link' );
    remove_action( 'wp_head', 'adjacent_posts_rel_link' );
    remove_action( 'wp_head', 'wp_shortlink_wp_head' );

    // Unregister unneeded navigation
    remove_action('genesis_after_header', 'genesis_do_subnav');

    // IE Only Meta Tag
    add_action('genesis_meta', 'ra_ie_meta_tag');

    // Remove default footer
    remove_action('genesis_footer', 'genesis_do_footer');
    add_action('genesis_footer', 'ra_do_footer');

    // Deregister footer widget area and reinstate
    remove_action('genesis_before_footer', 'genesis_footer_widget_areas');
    add_action('genesis_before_footer', 'ra_optin_box', 3);
    add_action('genesis_before_footer', 'genesis_footer_widget_areas', 4);

    // Remove unneeded WordPress and Genesis Widgets
    add_action('widgets_init', 'ra_remove_default_widgets', 11);

    // Custom Post Type Rewrite Rule
    add_action('init', 'ra_rewrite');

    // Remove 'Customize Page' link in Admin
    add_action('admin_init', 'ra_remove_customize');

    // Register Sidebar
    add_action('init', 'ra_register_sidebars');

    // Allow PHP Execution on Widgets
    add_filter('widget_text','ra_execute_php',100);

    // Add Shortcode Functionality to Text Widgets
	add_filter( 'widget_text', 'shortcode_unautop');
	add_filter( 'widget_text', 'do_shortcode');

    // Prevent WordPress from displaying login error message
    add_filter('login_errors', create_function('$a', "return null;"));

    // Remove Admin Bar
    add_filter('show_admin_bar', '__return_false');

    // Remove the edit link
    add_filter ( 'genesis_edit_post_link' , '__return_false' );

    //remove inline width and height added to images
    // add_filter( 'post_thumbnail_html', 'ra_remove_thumbnail_dimensions', 10 );
    // add_filter( 'image_send_to_editor', 'ra_remove_thumbnail_dimensions', 10 );

    // Removes attached image sizes as well
    // add_filter( 'the_content', 'ra_remove_thumbnail_dimensions', 10 );

    // Custom Body Class
    add_filter('body_class', 'ra_body_class');

    // Prevent Theme Update Check
    add_filter( 'http_request_args', 'ra_prevent_theme_update', 5, 2 );

    // Add Dashicons to Frontend
    // add_action( 'wp_enqueue_scripts', 'ra_dashicons_front_end' );

    // Remove WP Version
	add_filter('the_generator', 'ra_remove_wp_version');

	// HTML Editor as default
	add_filter( 'wp_default_editor', create_function('', 'return "html";') );

	// Prevent HTML on Comments
	add_filter( 'preprocess_comment', 'ra_comment_post', '', 1 );
	add_filter( 'comment_text', 'ra_comment_display', '', 1 );
	add_filter( 'comment_text_rss', 'ra_comment_display', '', 1 );
	add_filter( 'comment_excerpt', 'ra_comment_display', '', 1 );

	// This stops WordPress from trying to automatically make hyperlinks on text:
	remove_filter( 'comment_text', 'make_clickable', 9 );

	// High Quality JPEGs
	add_filter( 'jpg_quality', 'high_jpg_quality' );

    // Enqueue Google Web Fonts
    add_action('wp_print_styles', 'ra_google_webfont_enqueue');

    // Remove header-full-width
    add_filter('body_class', 'ra_remove_header_full_width');

    // Prevent WordPress from creating multiple image sizes
    add_filter('intermediate_image_sizes_advanced', 'ra_remove_image_sizes');

    // Force Full Width Layout
    add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

    if ('post' == get_post_type() && is_single()) {
    	add_filter('genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar');
    }

    // Remove Query Strings
    add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
    add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

    // Go To Top
    add_action('genesis_after_footer', 'ra_do_gototop');
}

function ra_do_gototop() {
    echo '<a href="#" class="gototop">Scroll</a>';
}

function ra_force_layout_single() {

}

// Remove query string from static files
function remove_cssjs_ver( $src ) {
    if( strpos( $src, '?ver=' ) )
    $src = remove_query_arg( 'ver', $src );
    return $src;
}

// Prevent WordPress from creating multiple image sizes function
function ra_remove_image_sizes( $sizes) {
    unset( $sizes['thumbnail']);
    unset( $sizes['medium']);
    unset( $sizes['large']);

    return $sizes;
}

// Remove Header Full Width Class
function ra_remove_header_full_width(array $classes) {
    foreach ($classes as $index => $class) {
        if ('header-full-width' === $class) {
            unset($classes[$index]);
            break;
        }
    }
    return $classes;
}

// Enqueue Google Web Font Function
function ra_google_webfont_enqueue() {
    wp_register_style('google-fonts', '//fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700,300italic|Open+Sans:400,400italic,300italic,300,700,700italic');
    wp_enqueue_style( 'google-fonts');
}

// High Quality JPEGs function
function high_jpg_quality() {
	return 100;
}

// This will occur when the comment is posted
function ra_comment_post( $incoming_comment ) {
    // convert everything in a comment to display literally
    $incoming_comment['comment_content'] = htmlspecialchars($incoming_comment['comment_content']);
    // the one exception is single quotes, which cannot be #039; because WordPress marks it as spam
    $incoming_comment['comment_content'] = str_replace( "'", '&apos;', $incoming_comment['comment_content'] );
    return( $incoming_comment );
}

// This will occur before a comment is displayed
function ra_comment_display( $comment_to_display ) {
    // Put the single quotes back in
    $comment_to_display = str_replace( '&apos;', "'", $comment_to_display );
    return $comment_to_display;
}

// Remove WP Version Function
function ra_remove_wp_version(){
	return '';
}

// Enqueue Dashicons Function
function ra_dashicons_front_end() {
    wp_enqueue_style( 'dashicons-style', get_stylesheet_uri(), array('dashicons'), '1.0' );
}


add_action( 'wp_enqueue_scripts', 'jk_load_dashicons' );
function jk_load_dashicons() {
    wp_enqueue_style( 'dashicons' );
}

// Prevent Theme Update Check Function
function ra_prevent_theme_update( $r, $url ) {
    if ( 0 !== strpos( $url, 'http://api.wordpress.org/themes/update-check' ) )
    return $r; // Not a theme update request. Bail immediately.
    $themes = unserialize( $r['body']['themes'] );
    //unset( $themes[ get_option( 'template' ) ] );
    unset( $themes[ get_option( 'stylesheet' ) ] );
    $r['body']['themes'] = serialize( $themes );
    return $r;
}

// Custom Body Class Function
function ra_body_class($classes) {
    if (is_home() || is_front_page()) {
        $classes[] = 'homepage';
    } else {
        $classes[] = 'innerpages';
    }
    return $classes;
}

// Remove Thumbnail Dimensions Function
function ra_remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}

// Allow PHP execution on widgets
function ra_execute_php($html){
    if(strpos($html,"<"."?php")!==false){
        ob_start();
        eval("?".">".$html);
        $html=ob_get_contents();
        ob_end_clean();
    }
    return $html;
}

//  IE & Chrome Meta Tag
function ra_ie_meta_tag() { ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="format-detection" content="telephone=no"/>
<?php }

function custom_style_sheet() {
wp_enqueue_style( 'custom-styling', get_stylesheet_directory_uri() . '/community-styles-ver-1.0.css' );
}
add_action('wp_enqueue_scripts', 'custom_style_sheet');

// Custom Footer
function ra_do_footer() { ?>
    <p>
        &copy; <?php _e('Copyright', 'genesis'); ?> Smart Marketer&trade; eCommerce Community. <?php _e('All Rights Reserved', 'genesis'); ?>. Smart Marketer &amp; Zipify are registered trademarks.
    </p>
<?php }

// Remove unneeded widgets
function ra_remove_default_widgets() {
    unregister_widget('WP_Widget_Pages');
    unregister_widget('WP_Widget_Calendar');
    unregister_widget('WP_Widget_Archives');
    unregister_widget('WP_Widget_Links');
    unregister_widget('WP_Widget_Meta');
    //unregister_widget('WP_Widget_Search');
    //unregister_widget('WP_Widget_Text');
    //unregister_widget('WP_Widget_Categories');
    //unregister_widget('WP_Widget_Recent_Posts');
    //unregister_widget('WP_Widget_Recent_Comments');
    unregister_widget('WP_Widget_RSS');
    unregister_widget('WP_Widget_Tag_Cloud');
    unregister_widget('WP_Nav_Menu_Widget');
    unregister_widget('Genesis_Featured_Page');
    unregister_widget('Genesis_User_Profile_Widget');
    unregister_widget('Genesis_Featured_Post');
}

// Register Sidebar Function
function ra_register_sidebars() {
	// Register Custom Sidebars
    genesis_register_sidebar( array(
        'name' => 'Home Featured',
        'id' => 'home-featured',
        'description' => __('Will be added after the header and navigation area.', 'genesis')
    ) );

    for ($sidebar = 1; $sidebar<=2; $sidebar++) {
        genesis_register_sidebar( array(
            'name' => 'Home Top '.$sidebar.'',
            'id' => 'home-top-'.$sidebar.'',
            'description' => ''
        ) );
    }

        genesis_register_sidebar( array(
            'name' => 'Community Nav Menu',
            'id' => 'community-nav-menu',
            'description' => __('Will be added at the bottom of the header', 'genesis')
        ) );

        genesis_register_sidebar( array(
            'name' => 'Utility Nav Menu',
            'id' => 'community-utility-nav',
            'description' => __('Will be added at the top of the header', 'genesis')
        ) );
        genesis_register_sidebar( array(
            'name' => 'Community Header',
            'id' => 'community-header-area',
            'description' => __('Will be added in header area', 'genesis')
        ) );
  	genesis_register_sidebar( array(
            'name' => 'Community Primary Navigation',
            'id' => 'community-main-nav',
            'description' => __('Will be added after header', 'genesis')
        ) );

        genesis_register_sidebar( array(
            'name' => 'Pincommerce Menu',
            'id' => 'pincommerce-menu',
            'description' => __('For the menu on the pincommerce-beta page.', 'genesis')
        ) );
        genesis_register_sidebar( array(
            'name' => 'Second Top Nav Menu',
            'id' => 'second-nav-menu',
            'description' => __('Will be added under the top nav menu.', 'genesis')
        ) );


    /*genesis_register_sidebar( array(
        'name' => 'Home Top',
        'id' => 'home-top',
        'description' => __('Will be added after the home featured area.', 'genesis')
    ) );*/

    genesis_register_sidebar( array(
        'name' => 'Home Middle',
        'id' => 'home-middle',
        'description' => __('Will replace the default loop on the home page if activated.', 'genesis')
    ) );

    genesis_register_sidebar( array(
        'name' => 'Home Bottom',
        'id' => 'home-bottom',
        'description' => __('Will be added after the site-inner areas.', 'genesis')
    ) );

    genesis_register_sidebar( array(
        'name' => 'Optin Box',
        'id' => 'optin-box',
        'description' => __('Will be added after the home bottom and footer widget area.', 'genesis')
    ) );

    genesis_register_sidebar( array(
        'id'            => 'category-page',
        'name'            => 'Category Page Sidebar',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'tabbed-sidebar',
        'name'            => 'Tabbed Sidebar',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'speakers',
        'name'            => 'Speakers',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'agenda',
        'name'            => 'Agenda',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'workshop',
        'name'            => 'Workshop',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'tickets',
        'name'            => 'Tickets',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'travel-accommodation',
        'name'            => 'Travel and Accommodation',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'testimonial',
        'name'            => 'Testimonial',
        'description'    => ''
    ) );

    genesis_register_sidebar( array(
        'id'            => 'faq',
        'name'            => 'FAQ',
        'description'    => ''
    ) );

    for ($press = 1; $press<=4; $press++) {
        genesis_register_sidebar( array(
            'name' => 'Press Page '.$press.'',
            'id' => 'press-page-'.$press.'',
            'description' => ''
        ) );
    }
}

// Remove Customize Admin Page
function ra_remove_customize() {
    remove_submenu_page('themes.php','customize.php');
}

function ra_optin_box() {
    if (is_active_sidebar('optin-box')) :
        genesis_markup(array(
            'html5' => '<div %s>',
            'xhtml' => '<div class="optin-box" id="optin-box">',
            'context' => 'optin-box'
        ));

        genesis_structural_wrap('optin-box');

            if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('optin-box')) {

            }

        genesis_structural_wrap('optin-box', 'close');

        echo '</div>';

    endif;
}

// Customize search form input box text
add_filter( 'genesis_search_text', 'ra_search_text' );
function ra_search_text( $text ) {
    return esc_attr( 'Search' );
}

// Remove Post Info and Meta on CPTs
add_action('genesis_meta', 'ra_remove_postmeta');
function ra_remove_postmeta() {
    $args = array(
       'public'   => true,
       '_builtin' => false
    );

    $output = 'names'; // names or objects, note names is the default
    $operator = 'and'; // 'and' or 'or'

    $post_types = get_post_types( $args, $output, $operator );

    foreach ( $post_types  as $post_type ) {
        if ($post_type == get_post_type() && is_single()) {
            remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
            remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
        }
    }
}
//add firestone footer sections with markup
// add_action('genesis_before_footer', 'ra_do_firestone_footer', 2);


add_action('genesis_after_header', 'ra_do_community_nav_menu', 5);
function ra_do_community_nav_menu(){
        if (is_active_sidebar('community-nav-menu')) {
            echo '<div class="community-nav-menu community-main-nav" id = "community-main-nav"><div class="wrap">';
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('community-nav-menu')) {
                }
            echo '</div></div>';
        }
}



add_action('genesis_before_header', 'ra_do_community_utility_nav', 5);
function ra_do_community_utility_nav(){
        if (is_active_sidebar('community-utility-nav')) {
            echo '<div class="community-utility-nav"><div class="wrap">';
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('community-utility-nav')) {
                }
            echo '</div></div>';
        }
}

add_action('genesis_header', 'ra_do_community_header', 5);
function ra_do_community_header(){
        if (is_active_sidebar('community-header-area')) {
            echo '<div class="community-header-area"><div class="wrap">';
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('community-header-area')) {
                }
            echo '</div></div>';
        }
}



add_action('genesis_after_header', 'ra_do_community_nav');
function ra_do_community_nav() {
        if (is_active_sidebar('community-main-nav')) {
            echo '<div class="community-main-nav" id="community-main-nav"><div class="wrap">';
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('community-main-nav')) {
                }
            echo '</div></div>';
        }
    }




function print_menu_shortcode($atts, $content = null) {
extract(shortcode_atts(array( 'name' => null, ), $atts));
return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
}
add_shortcode('menu', 'print_menu_shortcode');


// Add Genesis Support To Custom Post Type
add_action('init', 'atlweb_add_genesis_support');
function atlweb_add_genesis_support() {
    $args = array(
       'public'   => true,
       '_builtin' => false
    );
    $output = 'names'; // names or objects, note names is the default
    $operator = 'and'; // 'and' or 'or'
    $post_types = get_post_types( $args, $output, $operator );
    foreach ( $post_types  as $post_type ) {
        if ($post_type == get_post_type() && is_single()) {
            add_post_type_support( $post_type, 'genesis-seo' );
            add_post_type_support( $post_type, 'genesis-layouts' );
        }
    }
}

// Custom Rewrite Rule for Custom Post Types
function ra_rewrite() {
    global $wp_rewrite;
    $wp_rewrite->add_permastruct('typename', 'typename/%year%/%postname%/', true, 1);
    add_rewrite_rule('typename/([0-9]{4})/(.+)/?$', 'index.php?typename=$matches[2]', 'top');
    $wp_rewrite->flush_rules();
}

remove_action( 'genesis_meta', 'genesis_load_stylesheet' );

add_action( 'wp_enqueue_scripts', 'enqueue_font_awesome' );
function enqueue_font_awesome() {
wp_enqueue_style( 'font-awesome', '//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css', array(), '4.0.3' );
}



//add second top nav menu sections with markup
add_action('genesis_after_header', 'ra_do_second_nav');

function ra_do_second_nav() {
        if (is_active_sidebar('second-nav-menu')) {
            echo '<div class="second-nav-menu" id="second-top-nav-menu"><div class="wrap">';
                if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('second-nav-menu')) {
                }
            echo '</div></div>';
        }
    }

//add pincommerce menu
// add_action('genesis_after_entry', 'ra_do_pincommerce_menu');

// function ra_do_pincommerce_menu() {
// global $post;
// $postid = $post->ID;
//
// if ( (memb_hasAnyTags(302)) or (memb_hasAnyTags(340)) or (memb_hasAnyTags(318)) or (memb_hasAnyTags(310))){
// 	if (($postid==368) and (is_active_sidebar('pincommerce-menu'))){
//             echo '<div class="pincommerce-page-menu" id="pincommerce-page-menu">';
//                 if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('pincommerce-menu')) {
//                 }
//             echo '</div>';
//         }
//    }
// }

function mySearchFilter($query) {
  $post_type = 'page';
  if($query->is_main_query()){
    if ($query->is_search) {
      $query->set('post_type', $post_type);
    }
  }
};
add_action('pre_get_posts','mySearchFilter');


add_action('genesis_footer', 'ra_do_enqueue_intercom');
function ra_do_enqueue_intercom(){
	wp_enqueue_scripts('intercom-script', 'http://community.smartmarketer.com/wp-content/uploads/2015/12/Intercom.js');
}
