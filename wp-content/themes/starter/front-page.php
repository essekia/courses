<?php
/**
 * @Template Home
 *
 * @package      Starter for Genesis
 * @since        0.1
 * @link         http://www.superfastbusiness.com
 * @author       SuperFastBusiness <www.superfastbusiness.com>
 * @copyright    Copyright (c) 2014, SuperFastBusiness
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

// Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Set global settings before execution
add_action('genesis_meta', 'ra_do_meta');
function ra_do_meta() {
    // Home Featured
    if (is_active_sidebar('Home Featured')) {
    	add_action('genesis_after_header', 'ra_do_home_featured');
    }

    // Home Top
    if (is_active_sidebar('Home Top 1') || is_active_sidebar('Home Top 2')) {
    	// add_action('genesis_after_header', 'ra_do_home_top');
    }

    // Home Middle
    if (is_active_sidebar('Home Middle')) {
    	remove_action('genesis_loop', 'genesis_do_loop');
    	add_action('genesis_loop', 'ra_do_home_middle');
    }

    // Home Bottom
    if (is_active_sidebar('Home Bottom')) {
    	// add_action('genesis_before_footer', 'ra_do_home_bottom', 2);
    }
}

// Home Featured
function ra_do_home_featured() {
	if (is_active_sidebar('Home Featured')) :

		genesis_markup( array(
			'html5' => '<div %s>',
			'xhtml' => '<div id="home-featured" class="home-featured">',
			'context' => 'home-featured',
		) );

		genesis_structural_wrap('home-featured');

		if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Featured')) {

		}

		genesis_structural_wrap('home-featured', 'close');
		echo '</div>';

	endif;
}

// Slider / Call to Action Area
function ra_do_home_top() {
	if (is_active_sidebar('Home Top 1') || is_active_sidebar('Home Top 2')) :

		genesis_markup( array(
			'html5' => '<div %s>',
			'xhtml' => '<div id="home-top" class="home-top">',
			'context' => 'home-top',
		) );

		genesis_structural_wrap('home-top');

		echo '<div class="home-top-1">';

		if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Top 1')) {

		}

		echo '</div>';
		

		genesis_structural_wrap('home-top', 'close');

		echo '</div>';

	endif;
}

// Replace the default blog post with a welcome message or custom widget
function ra_do_home_middle() {
	if (is_active_sidebar('Home Middle')) :
		genesis_markup(array(
			'html5' => '<div %s>',
			'xhtml' => '<div id="home-middle" class="home-middle">',
			'context' => 'home-middle'
		));

		global $post;
		$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
		$args = array(
			'posts_per_page' => 15,
			'paged' => $paged,
		);

		query_posts( $args );

		if ( have_posts() ) :
			while ( have_posts() ) : the_post(); ?>
				<div <?php post_class(); ?>>
					<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
					<a class="aligncenter">
						<?php $image = ra_post_image();
						if ($image) {
							$image = aq_resize($image, 300);
						} ?>
						<?php if (!empty($image)) { ?>
							<a class="aligncenter" href="<?php the_permalink(); ?>">
								<img class="aligncenter" src="<?php echo $image; ?>" alt="<?php the_title(); ?>" />
							</a>
						<?php } ?>
					</a>
					<span class="readmore">
						<a href="<?php the_permalink(); ?>"><?php _e('View Post &raquo;'); ?></a>
					</span>
				</div>
			<?php endwhile;
			// paginate();
			do_action( 'genesis_after_endwhile' );
		endif;

		echo '</div>';
	endif;
}

// Bottom call to action area / custom widget
function ra_do_home_bottom() {
	if (is_active_sidebar('Home Bottom')) :

		genesis_markup( array(
			'html5' => '<div %s>',
			'xhtml' => '<div id="home-bottom" class="home-bottom">',
			'context' => 'home-bottom',
		) );

		genesis_structural_wrap('home-bottom');

		if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Bottom')) {

		}

		genesis_structural_wrap('home-bottom', 'close');
		echo '</div>';

	endif;
}

add_action( 'wp_footer', 'ra_home_script', 99 );
function ra_home_script() { ?>
	<script type="text/javascript">
		jQuery(function($) {
			var $container = $('.home-middle');

			$container.gridalicious({
				selector: '.post',
				gutter: 20,
				width: 300
			});



			$container.infinitescroll({
			  navSelector  : '.pagination',
			  nextSelector : '.pagination-next a',
			  itemSelector : '.post',
			  contentSelector :'.home-middle',
			  loading: {
			      finishedMsg: 'No more pages to load.',
			      img: null
			    }
			  },
			  function( newElements ) {
			    var $newElems = $( newElements ).css({ opacity: 0 });
					$newElems.animate({ opacity: 1 });
					$container.gridalicious( 'append', $newElems );
			  }
			);
		});
	</script>
<?php }

genesis();