<?php
/**
 * Template Name: Press v2
 *
 * @package      Starter for Genesis
 * @since        0.1
 * @link         http://www.superfastbusiness.com
 * @author       SuperFastBusiness <www.superfastbusiness.com>
 * @copyright    Copyright (c) 2014, SuperFastBusiness
 * @license      http://opensource.org/licenses/gpl-2.0.php GNU Public License
 *
 */

add_action( 'genesis_meta', 'ra_press_meta' );
function ra_press_meta() {
	if ( is_active_sidebar( 'press-page-1' ) || is_active_sidebar( 'press-page-2' ) || is_active_sidebar( 'press-page-3' ) || is_active_sidebar( 'press-page-4' ) )
		add_action( 'genesis_after_entry_content', 'ra_press_sidebar' );
}

function ra_press_sidebar() {
	if ( is_active_sidebar( 'press-page-1' ) )
		dynamic_sidebar( 'press-page-1' );

	if ( is_active_sidebar( 'press-page-2' ) )
		dynamic_sidebar( 'press-page-2' );

	if ( is_active_sidebar( 'press-page-3' ) )
		dynamic_sidebar( 'press-page-3' );

	if ( is_active_sidebar( 'press-page-4' ) )
		dynamic_sidebar( 'press-page-4' );
}

genesis();