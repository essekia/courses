<?php

add_action('genesis_entry_content', 'ra_single_course_content');
function ra_single_course_content() {
	global $post;
	$video = get_post_meta($post->ID, '_ra_course_video', true);
	echo '<div class="video">';
		echo (!empty($video) ? $video : '');
	echo '</div>';
}

genesis();