<?php

//$cat = get_queried_object();
//print_r($cat);
add_action('genesis_before_loop', 'ra_category_loop');
function ra_category_loop() {
	if (is_active_sidebar('category-page')) {
		echo '<h2 class="entry-title">'.__('Category', 'genesis').'</h2>';
		echo '<div class="category-lists">';
			
			if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('category-page')) {

			}

		echo '</div>';	
	}
}

remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', 'ra_category_post_loop');

function ra_category_post_loop() {
	$term = get_queried_object();
	global $post;
	$slug = $term->slug;

	$args = array(
		'post_type' => 'post',
		'posts_per_page' => -1,
		'tax_query' => array(
			array(
				'taxonomy' => $term->taxonomy,
				'field' => 'slug',
				'terms' => $slug
			)
		)
	);

	$loop = new WP_Query($args);

	//var_dump($loop);
	if ($loop->have_posts()) :
		while ($loop->have_posts()) : $loop->the_post();
			$image = ra_post_image();
			if (!empty($image)) {
				$image = aq_resize($image, 300, 180);
			} else {
				$image = 'http://placehold.it/300x180';
			}
			echo '<div class="category-list post-'.$post->ID.'">';
				echo '<div class="one-third first title"><h2 class="post-title"><a href="'.get_permalink().'">'.get_the_title().'</a></h2></div>';
				echo '<div class="one-third image"><a href="'.get_permalink().'"><img src="'.$image.'" alt="'.get_the_title().'"/></a></div>';
				echo '<div class="one-third link"><a class="more-link" href="'.get_permalink().'">'.__('View Post', 'genesis').'</a></div>';
			echo '</div>';
		endwhile;
		wp_reset_postdata();
	else :
		_e('Sorry, no posts matched your criteria.');
	endif;
}

genesis();